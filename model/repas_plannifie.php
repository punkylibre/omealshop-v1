<?php
require_once 'repas.php';
require_once 'foyer.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:47
 */
class repas_plannifie
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var bool|foyer
     */
    private $foyer;
    /**
     * @var bool|repas
     */
    private $repas;
    /**
     * @var bool|personne
     */
    private $demandeur;
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var int
     */
    private $nb_personnes;

    /**
     * repas_plannifie constructor.
     * @param $id int
     * @param $foyer foyer
     * @param $repas repas
     * @param $date DateTime
     * @param $nb_personnes int
     */
    public function __construct($id, $foyer, $repas, $demandeur, $date, $nb_personnes)
    {
        $this->id = $id;
        $this->foyer = foyer::getById($foyer);
        $this->repas = repas::getById($repas);
        $this->demandeur = personne::getById($demandeur);
        $this->date = new DateTime($date);
        $this->nb_personnes = $nb_personnes;
    }

    /**
     * @param $id
     * @return bool|repas_plannifie
     */
    public static function getById($id)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM repas_plannifie WHERE REPAS_PLANNIFIE_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if ($res = $reqGetById->fetch())
            return new repas_plannifie($res['REPAS_PLANNIFIE_id'],
                $res['REPAS_PLANNIFIE_foyer_id'],
                $res['REPAS_PLANNIFIE_repas_id'],
                $res['REPAS_PLANNIFIE_demandeur'],
                $res['REPAS_PLANNIFIE_date'],
                $res['REPAS_PLANNIFIE_nb_personnes']);
        else
            return false;
    }

    /**
     * @param $foyer
     * @param $repas
     * @param $personne
     * @param $date
     * @param $nbPersonnes
     * @return bool
     */
    public static function addRepas($foyer, $repas, $personne, $date, $nbPersonnes)
    {
        $reqInsertRepas = PDO_OMealShop::connexionBDD()->prepare("INSERT INTO repas_plannifie(REPAS_PLANNIFIE_foyer_id,
                    REPAS_PLANNIFIE_repas_id,
                    REPAS_PLANNIFIE_demandeur,
                    REPAS_PLANNIFIE_date,
                    REPAS_PLANNIFIE_nb_personnes) 
                    VALUES(:idFoyer, :idRepas, :idPersonne, :dateRep, :nbPers);");
        $reqInsertRepas->execute(array(':idRepas' => $repas->getId(), ':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId(), ':dateRep' => date_format($date, 'Y-m-d H:i:s'), ':nbPers' => $nbPersonnes));
        if($reqInsertRepas->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param $foyer
     * @param $date
     * @return bool|repas_plannifie
     */
    public static function repasAlreadyPlanned($foyer, $date){
        $reqRepasExistant = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas_plannifie  
                          WHERE REPAS_PLANNIFIE_foyer_id = :foyer AND REPAS_PLANNIFIE_date = :dateRep;");
        $reqRepasExistant->execute(array(':foyer' => $foyer->getId(), ':dateRep' => $date));
        if ($res = $reqRepasExistant->fetch()) {
            return new repas_plannifie($res['REPAS_PLANNIFIE_id'],
                $res['REPAS_PLANNIFIE_foyer_id'],
                $res['REPAS_PLANNIFIE_repas_id'],
                $res['REPAS_PLANNIFIE_demandeur'],
                $res['REPAS_PLANNIFIE_date'],
                $res['REPAS_PLANNIFIE_nb_personnes']);
        }
        return false;
    }

    /**
     * @param $dateDeb
     * @param $dateFin
     * @param $foyer
     * @return array
     */
    public static function getRepasBetweenDate($dateDeb, $dateFin, $foyer){
        $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas_plannifie
                                                    WHERE REPAS_PLANNIFIE_date BETWEEN :dateDeb AND :dateFin AND REPAS_PLANNIFIE_foyer_id = :foyer;");
        $reqGetRepas->execute(array(':dateDeb' => $dateDeb, ':dateFin' => $dateFin, ':foyer' => $foyer->getId()));
        $resultats = $reqGetRepas->fetchAll();
        foreach ($resultats as $res) {
            $repas[] = new repas_plannifie($res['REPAS_PLANNIFIE_id'],
                $res['REPAS_PLANNIFIE_foyer_id'],
                $res['REPAS_PLANNIFIE_repas_id'],
                $res['REPAS_PLANNIFIE_demandeur'],
                $res['REPAS_PLANNIFIE_date'],
                $res['REPAS_PLANNIFIE_nb_personnes']);
        }
        return $repas;
    }

    /**
     * @param $foyer
     * @param $date
     * @param $nbPersonnes
     * @param $repas
     * @return bool
     */
    public static function updateRepas($foyer, $date, $nbPersonnes, $repas){
        $reqInsertRepas = PDO_OMealShop::connexionBDD()->prepare("UPDATE repas_plannifie SET REPAS_PLANNIFIE_repas_id = :repas, REPAS_PLANNIFIE_nb_personnes = :nbPers 
                            WHERE REPAS_PLANNIFIE_foyer_id = :foyer AND REPAS_PLANNIFIE_date = :dateRep;");
        $reqInsertRepas->execute(array(':repas' => $repas->getId(), ':foyer' => $foyer->getId(), ':dateRep' => $date, ':nbPers' => $nbPersonnes));
        if ($reqInsertRepas->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param $foyer
     * @param $date
     * @return bool
     */
    public static function supprRepas($foyer, $date){
        $reqSupprRepas = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM repas_plannifie WHERE REPAS_PLANNIFIE_foyer_id = :foyer AND REPAS_PLANNIFIE_date = :dateRep;");
        $reqSupprRepas->execute(array(':foyer' => $foyer->getId(), ':dateRep' => $date));
        if ($reqSupprRepas->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param foyer $foyer
     * @return bool
     */
    public static function supprAncienRepas($foyer){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM repas_plannifie WHERE REPAS_PLANNIFIE_date < :dateRep');
        $date = new DateTime('-'.$foyer->getTempsSuppr().' week');
        $reqGetById->execute(array(':dateRep' => date_format($date, 'y-m-d h:m:s')));
        if($res = $reqGetById->rowCount())
            return true;
        return false;
    }

    /**
     * @return bool
     */
    public function supprimer(){
        $reqSupprRepas = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM repas_plannifie WHERE REPAS_PLANNIFIE_id = :id");
        $reqSupprRepas->execute(array(':id' => $this->id));
        if ($reqSupprRepas->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @return array
     */
    public function getIngredients(){
        $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas_plannifie RP JOIN ingredients_repas IR ON RP.REPAS_PLANNIFIE_repas_id = IR.INGREDIENTS_REPAS_repas_id 
                                                    WHERE INGREDIENTS_REPAS_repas_id = :idRepas AND REPAS_plannifie_id = :idRepasPlan;");
        $reqGetRepas->execute(array(':idRepas' => $this->repas->getId(), ':idRepasPlan' => $this->id));
        $resultats = $reqGetRepas->fetchAll();
        foreach ($resultats as $res) {
            $ingredients_repas[] = new ingredients_repas($res['INGREDIENTS_REPAS_id'],
                $res['INGREDIENTS_REPAS_repas_id'],
                $res['INGREDIENTS_REPAS_ingredients_id'],
                $res['INGREDIENTS_REPAS_quantite']);
        }
        return $ingredients_repas;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool|repas
     */
    public function getRepas()
    {
        return $this->repas;
    }

    /**
     * @return int
     */
    public function getNbPersonnes()
    {
        return $this->nb_personnes;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return bool|personne
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }
}
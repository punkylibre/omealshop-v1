<?php
require_once 'foyer.php';
require_once 'categorie_repas.php';
require_once 'session.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:31
 */
class repas
{
    private $id;
    private $nom;
    private $createur;
    private $categorie_repas;
    private $type_repas;

    /**
     * repas constructor.
     * @param $id int
     * @param $nom string
     * @param $createur int
     * @param $categorie_repas int
     */
    public function __construct($id, $nom, $createur, $categorie_repas, $type_repas)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->createur = $createur;
        $this->categorie_repas = categorie_repas::getById($categorie_repas);
        $this->type_repas = $type_repas;
    }

    public static function getById($id)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM repas WHERE REPAS_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if ($res = $reqGetById->fetch())
            return new repas($res['REPAS_id'],
                $res['REPAS_nom'],
                $res['REPAS_createur'],
                $res['REPAS_categorie_repas_id'],
                $res['REPAS_type_repas']);
        else
            return false;
    }

    public static function getByNomFoyer($nomRepas, $foyerId)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM repas WHERE REPAS_nom = :nom AND REPAS_createur = :idFoyer');
        $reqGetById->execute(array(':nom' => $nomRepas, ':idFoyer' => $foyerId));
        if ($res = $reqGetById->fetch())
            return new repas($res['REPAS_id'],
                $res['REPAS_nom'],
                $res['REPAS_createur'],
                $res['REPAS_categorie_repas_id'],
                $res['REPAS_type_repas']);
        else
            return false;
    }

    public static function getRepasByTypeAndCat($type, $cat)
    {
        $session = unserialize($_SESSION['session']);
        $foyer = $session->getFOyer();
        if ($cat == 'tout') {
            $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas WHERE REPAS_type_repas = :type AND (REPAS_createur IS NULL OR REPAS_createur = :idFoyer);");
            $reqGetRepas->execute(array(':type' => $type, ':idFoyer' => $foyer->getId()));
        } else {
            $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas WHERE REPAS_type_repas = :type AND REPAS_categorie_repas_id = :idCat AND (REPAS_createur IS NULL OR REPAS_createur = :idFoyer);");
            $reqGetRepas->execute(array(':type' => $type, ':idFoyer' => $foyer->getId(), 'idCat' => $cat));
        }
        $resultats = $reqGetRepas->fetchAll();
        foreach ($resultats as $res) {
            if (self::getByNomFoyer($res['REPAS_nom'], $foyer->getId()) && $res['REPAS_id'] != self::getByNomFoyer($res['REPAS_nom'], $foyer->getId())->getId())
                continue;
            $repas[] = new repas($res['REPAS_id'],
                $res['REPAS_nom'],
                $res['REPAS_createur'],
                $res['REPAS_categorie_repas_id'],
                $res['REPAS_type_repas']);
        }
        return $repas;
    }

    public static function getRepasByCat($cat)
    {
        $session = unserialize($_SESSION['session']);
        $foyer = $session->getFOyer();
        if ($cat == 'tout') {
            $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas WHERE (REPAS_createur IS NULL OR REPAS_createur = :idFoyer) ORDER BY REPAS_type_repas;");
            $reqGetRepas->execute(array(':idFoyer' => $foyer->getId()));
        } else {
            $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM repas WHERE REPAS_categorie_repas_id = :idCat AND (REPAS_createur IS NULL OR REPAS_createur = :idFoyer) ORDER BY REPAS_type_repas;");
            $reqGetRepas->execute(array(':idFoyer' => $foyer->getId(), 'idCat' => $cat));
        }
        $resultats = $reqGetRepas->fetchAll();
        foreach ($resultats as $res) {
            if (self::getByNomFoyer($res['REPAS_nom'], $foyer->getId()) && $res['REPAS_id'] != self::getByNomFoyer($res['REPAS_nom'], $foyer->getId())->getId())
                continue;
            $repas[] = new repas($res['REPAS_id'],
                $res['REPAS_nom'],
                $res['REPAS_createur'],
                $res['REPAS_categorie_repas_id'],
                $res['REPAS_type_repas']);
        }
        foreach ($repas as $repa) {

        }
        return $repas;
    }

    public static function getRepasByFoyer($foyerId){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM repas WHERE REPAS_createur = :idFoyer');
        $reqGetById->execute(array(':idFoyer' => $foyerId));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
             $repas[] = new repas($res['REPAS_id'],
                $res['REPAS_nom'],
                $res['REPAS_createur'],
                $res['REPAS_categorie_repas_id'],
                $res['REPAS_type_repas']);
        }
        return $repas;
    }

    public static function addRepas($nomRepas, $foyer, $categorieRepas, $typeRepas)
    {
        $reqInsertRepas = PDO_OMealShop::connexionBDD()->prepare("INSERT INTO repas (REPAS_nom, REPAS_createur, REPAS_categorie_repas_id, REPAS_type_repas)
                    VALUES(:nom, :createur, :categorie, :typeRepas);");
        $reqInsertRepas->execute(array(':nom' => $nomRepas, ':createur' => $foyer, ':categorie' => $categorieRepas, ':typeRepas' => $typeRepas));
        if ($reqInsertRepas->rowCount() > 0)
            return true;
        return false;
    }

    public static function existeDeja($nomRepas, $idFoyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM repas WHERE REPAS_nom = :nom AND REPAS_createur = :idFoyer;');
        $reqGetById->execute(array(':nom' => $nomRepas, ':idFoyer' => $idFoyer));
        if ($res = $reqGetById->fetch()) {
            return true;
        }
        return false;
    }

    public function getIngredients()
    {
        $reqGetRepas = PDO_OMealShop::connexionBDD()->prepare("SELECT * FROM ingredients_repas
                                                    WHERE INGREDIENTS_REPAS_repas_id = :idRepas;");
        $reqGetRepas->execute(array(':idRepas' => $this->getId()));
        $resultats = $reqGetRepas->fetchAll();
        foreach ($resultats as $res) {
            $ingredients_repas[] = new ingredients_repas($res['INGREDIENTS_REPAS_id'],
                $res['INGREDIENTS_REPAS_repas_id'],
                $res['INGREDIENTS_REPAS_ingredients_id'],
                $res['INGREDIENTS_REPAS_quantite']);
        }
        return $ingredients_repas;
    }

    public function suppr(){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM repas WHERE REPAS_id = :id');
        $reqGetById->execute(array(':id' => $this->id));
        if($res = $reqGetById->rowCount())
            return true;
        return false;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool|categorie_repas
     */
    public function getCategorieRepas()
    {
        return $this->categorie_repas;
    }

    /**
     * @return foyer
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * @return mixed
     */
    public function getTypeRepas()
    {
        return $this->type_repas;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        if($this->nom == $nom)
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE repas SET REPAS_nom = :val WHERE REPAS_id = :id');
        $reqUpdate->execute(array(':id' => $this->id, ':val' => $nom));
        $this->nom = $nom;
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param bool|categorie_repas $categorie_repas
     */
    public function setCategorieRepas($categorie_repas)
    {
        if($this->categorie_repas->getId() == $categorie_repas->getId())
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE repas SET REPAS_categorie_repas_id = :val WHERE REPAS_id = :id');
        $reqUpdate->execute(array(':id' => $this->id, ':val' => $categorie_repas->getId()));
        $this->categorie_repas = $categorie_repas;
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param mixed $type_repas
     */
    public function setTypeRepas($type_repas)
    {
        if($this->type_repas == $type_repas)
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE repas SET REPAS_type_repas = :val WHERE REPAS_id = :id');
        $reqUpdate->execute(array(':id' => $this->id, ':val' => $type_repas));
        $this->type_repas = $type_repas;
        if ($reqUpdate->rowCount() > 0)
            return true;
        return false;
    }
}
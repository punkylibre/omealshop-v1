<?php
require_once '../model/DB.php';
require_once '../model/personne.php';
require_once '../model/foyer.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 24/06/17
 * Time: 10:34
 */
class session
{
    /**
     * @var personne
     */
    private $personne;
    /**
     * @var foyer|null
     */
    private $foyer = null;

    /**
     * session constructor.
     * @param $personne personne
     */
    public function __construct($personne)
    {
        $this->personne = $personne;
    }

    /**
     * @return personne
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * @return foyer|null
     */
    public function getFoyer()
    {
        return $this->foyer;
    }

    /**
     * @param foyer|null $foyer
     */
    public function setFoyer($foyer)
    {
        $this->foyer = $foyer;
    }

    /**
     * @param personne $personne
     */
    public function setPersonne($personne)
    {
        $this->personne = $personne;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 19:51
 */
require_once("../config.inc.php");


/**
 * Class PDO_etude
 */
class PDO_OMealShop{
    /**
     * @var null|PDO
     * @description connexion au schéma db_etude
     */
    private static $pdo_omealshop = null;

    /**
     * PDO_etude constructor.
     * @description initialise la connexion au schéma omealshop_db,
     * @note Constructeur privé pour ne pas pouvoir être appelé et éviter l'initialisation de plusieurs connexion
     */
    private function __construct(){
        // Création des variables de connexion PDO (PHP Data Object)
        $user = DB_USER_OMEALSHOP;
        $pwd  = DB_PSWD_OMEALSHOP;
        $dsn  = DB_TYPE.':host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_BASE_OMEALSHOP;

        $pdo = null;
        try{
            $pdo = new PDO($dsn, $user, $pwd);
            if(isset($pdo)){
                self::$pdo_omealshop = $pdo;
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo->exec('SET CHARACTER SET utf8');
            }
        }
        catch(PDOException $pe){
            echo $pe->getMessage();
            die();       //vérifier qu'on s'est bien connecté à la base
        }
    }

    /**
     * @name connexionBDD()
     * @return null|PDO
     * @description Retourne la connexion unique à la base de donnée sur le schéma db_etude
     */
    public static function connexionBDD(){
        if(self::$pdo_omealshop == null){
            new PDO_OMealShop();
        }
        return self::$pdo_omealshop;
    }
}
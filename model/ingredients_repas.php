<?php

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:42
 */
class ingredients_repas
{
    private $id;
    private $repas;
    private $ingredients;
    private $quantite;

    /**
     * ingredients_repas constructor.
     * @param $id int
     * @param $repas repas
     * @param $ingredients ingredients
     * @param $quantite int
     */
    public function __construct($id, $repas, $ingredients, $quantite)
    {
        $this->id = $id;
        $this->repas = repas::getById($repas);
        $this->ingredients = ingredients::getById($ingredients);
        $this->quantite = $quantite;
    }

    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM ingredients_repas WHERE INGREDIENTS_REPAS_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new ingredients_repas($res['INGREDIENTS_REPAS_id'],
                $res['INGREDIENTS_REPAS_repas_id'],
                $res['INGREDIENTS_REPAS_ingredients_id'],
                $res['INGREDIENTS_REPAS_quantite']);
        else
            return false;
    }

    public static function addIngredient($idRepas, $ingredient, $quantite)
    {
        $reqInsertIngredient = PDO_OMealShop::connexionBDD()->prepare("INSERT INTO ingredients_repas (INGREDIENTS_REPAS_repas_id, INGREDIENTS_REPAS_ingredients_id, INGREDIENTS_REPAS_quantite)
                    VALUES(:idRepas, :idIngredient, :quantite);");
        $reqInsertIngredient->execute(array(':idRepas' => $idRepas, ':idIngredient' => $ingredient, ':quantite' => $quantite));
        if ($reqInsertIngredient->rowCount() > 0)
            return true;
        return false;
    }

    public function remove(){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM ingredients_repas WHERE INGREDIENTS_REPAS_id = :id');
        $reqGetById->execute(array(':id' => $this->id));
        if($res = $reqGetById->rowCount())
            return true;
        return false;
    }

    /**
     * @return bool|ingredients
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @return bool|repas
     */
    public function getRepas()
    {
        return $this->repas;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        if($this->quantite == $quantite)
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE ingredients_repas SET INGREDIENTS_REPAS_quantite = :val WHERE INGREDIENTS_REPAS_id = :id;');
        $reqUpdate->execute(array(':id' => $this->id, ':val' => $quantite));
        if ($reqUpdate->rowCount() > 0) {
            $this->nom = $quantite;
            return true;
        }
        return false;
    }
}
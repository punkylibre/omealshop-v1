<?php
require_once 'DB.php';
require_once 'personne.php';
require_once 'foyer.php';

/**
 * Class membre_foyer
 */
class membre_foyer
{
    /**
     * @var personne
     */
    private $personne;
    /**
     * @var foyer
     */
    private $foyer;

    private $etat;

    private $foyerDefault;

    /**
     * membre_foyer constructor.
     * @param personne $personne
     * @param foyer $foyer
     */
    public function __construct($personne, $foyer, $etat, $default)
    {
        $this->personne = personne::getById($personne);
        $this->foyer = foyer::getById($foyer);
        $this->etat = $etat;
        $this->foyerDefault = $default;
    }

    public static function getById($idPersonne, $idFoyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM membre_foyer WHERE MEMBRE_FOYER_personne_id = :idPersonne AND MEMBRE_FOYER_foyer_id = :idFoyer;');
        $reqGetById->execute(array(':idPersonne' => $idPersonne, ':idFoyer' => $idFoyer));
       if($res = $reqGetById->fetch()){
           return new membre_foyer($res['MEMBRE_FOYER_personne_id'], $res['MEMBRE_FOYER_foyer_id'], $res['MEMBRE_FOYER_etat'], $res['MEMBRE_FOYER_default']);
       }
       return false;
    }

    public static function getFoyersPersonne($idPersonne)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM membre_foyer WHERE MEMBRE_FOYER_personne_id = :idPersonne AND MEMBRE_FOYER_etat = 1');
        $reqGetById->execute(array(':idPersonne' => $idPersonne));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            $foyers[] = foyer::getById($res['MEMBRE_FOYER_foyer_id']);
        }
        return $foyers;
    }

    public static function getMembresFoyer($idFoyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM membre_foyer WHERE MEMBRE_FOYER_foyer_id = :idFoyer AND MEMBRE_FOYER_etat = 1');
        $reqGetById->execute(array(':idFoyer' => $idFoyer));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            $membres[] = personne::getById($res['MEMBRE_FOYER_personne_id']);
        }
        return $membres;
    }

    public static function getDemandesMembres($idFoyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM membre_foyer WHERE MEMBRE_FOYER_foyer_id = :idFoyer AND MEMBRE_FOYER_etat = 0');
        $reqGetById->execute(array(':idFoyer' => $idFoyer));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            $membres[] = personne::getById($res['MEMBRE_FOYER_personne_id']);
        }
        return $membres;
    }

    public static function addMembre($foyer, $personne)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('INSERT INTO membre_foyer(MEMBRE_FOYER_personne_id, MEMBRE_FOYER_foyer_id, MEMBRE_FOYER_etat) VALUES(:idPersonne, :idFoyer, 1);');
        $reqInsert->execute(array(':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId()));
        if ($reqInsert->rowCount() > 0) {
            $destinataire = $personne->getMail();
            $sujet = "Ajouter à un foyer";
            $entete = "From: no_reply@omealshop.com \n";
            $message = 'Bonjour,
		    
Vous venez d\'être ajouter au foyer suivant : '.$foyer->getNom().', par '.$foyer->getPersonneCreateur()->getPrenom().' '.$foyer->getPersonneCreateur()->getNom().' 
Vous pouvez vous occupez de ce foyer en vous connectant  sur le site
'.TXT_addresse_site.'/index.php
            
---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';

            return mail($destinataire, $sujet, $message, $entete); // Envoi du mail
        }
        return false;
    }

    public static function addCandidat($foyer, $personne)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('INSERT INTO membre_foyer(MEMBRE_FOYER_personne_id, MEMBRE_FOYER_foyer_id, MEMBRE_FOYER_etat) VALUES(:idPersonne, :idFoyer, 0);');
        $reqInsert->execute(array(':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId()));
        if ($reqInsert->rowCount() > 0) {
            $destinataire = $foyer->getPersonneCreateur()->getMail();
            $sujet = "Demande de rejoindre votre foyer";
            $entete = "From: no_reply@omealshop.com \n";
            $message = 'Bonjour,
		    
Un membre de OMealShop souhaiterais rejoindre votre foyer : '.$foyer->getNom().'.
Il s\'agit de '.$personne->getPrenom().' '.$personne->getNom().'.
Allez dans l\'onglet "Foyer" puis cliquez sur "gestion des foyers" afin de valider sa demande 
'.TXT_addresse_site.'/index.php
            
---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';

            return mail($destinataire, $sujet, $message, $entete); // Envoi du mail
        }
        return false;
    }

    public static function supprMembre($idFoyer, $idPersonne)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM membre_foyer WHERE MEMBRE_FOYER_foyer_id = :idFoyer AND MEMBRE_FOYER_personne_id = :idPersonne;');
        $reqInsert->execute(array(':idFoyer' => $idFoyer, ':idPersonne' => $idPersonne));
        if ($reqInsert->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public static function supprFoyer($idFoyer)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM membre_foyer WHERE MEMBRE_FOYER_foyer_id = :idFoyer;');
        $reqInsert->execute(array(':idFoyer' => $idFoyer));
        if ($reqInsert->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public static function getFoyerParDefaut($idPersonne)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM membre_foyer WHERE MEMBRE_FOYER_personne_id = :idPersonne;');
        $reqGetById->execute(array(':idPersonne' => $idPersonne));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            if($res['MEMBRE_FOYER_default'] == 1)
                return foyer::getById($res['MEMBRE_FOYER_foyer_id']);
        }
        return null;
    }

    public static function setDefault($foyer, $personne)
    {
        $flag = false;
        if(membre_foyer::getFoyerParDefaut($personne->getId()) != null) {
            $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE membre_foyer SET MEMBRE_FOYER_default = 0 WHERE MEMBRE_FOYER_personne_id = :idPersonne;');
            $reqUpdate->execute(array(':idPersonne' => $personne->getId()));
            if ($reqUpdate->rowCount() > 0) {
                $flag = true;
            }
            else
                $flag = false;
        }
        else
            $flag = true;

        if($flag){
            $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE membre_foyer SET MEMBRE_FOYER_default = 1 WHERE MEMBRE_FOYER_foyer_id = :idFoyer AND MEMBRE_FOYER_personne_id = :idPersonne;');
            $reqUpdate->execute(array(':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId()));
            if ($reqUpdate->rowCount() > 0) {
                return true;
            }
            return false;
        }
    }

    /**
     * @return foyer
     */
    public function getFoyer()
    {
        return $this->foyer;
    }

    /**
     * @return personne
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * @param int $etat
     */
    public function setEtat($etat)
    {
        if($this->etat == $etat)
            return true;
        if($etat == 1){
            $destinataire = $this->personne->getMail();
            $sujet = "Ajouter à un foyer";
            $entete = "From: no_reply@omealshop.com \n";
            $message = 'Bonjour,
            
Votre demande vient d\'être acceptée pour rejoindre le foyer suivant : '.$this->foyer->getNom().', par '.$this->foyer->getPersonneCreateur()->getPrenom().' '.$this->foyer->getPersonneCreateur()->getNom().' 
Vous pouvez vous occupez de ce foyer en vous connectant  sur le site
'.TXT_addresse_site.'/index.php
            
---------------
Ceci est un mail automatique, Merci de ne pas y répondre.';

            mail($destinataire, $sujet, $message, $entete); // Envoi du mail
        }
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE membre_foyer SET MEMBRE_FOYER_etat = :etat WHERE MEMBRE_FOYER_foyer_id = :idFoyer AND MEMBRE_FOYER_personne_id = :idPersonne;');
        $reqUpdate->execute(array(':idFoyer' => $this->foyer->getId(), ':idPersonne' => $this->personne->getId(), ':etat' => $etat));
        if ($reqUpdate->rowCount() > 0) {
            $this->etat = $etat;
            return true;
        }
        return false;
    }


}
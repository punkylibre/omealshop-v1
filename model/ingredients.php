<?php
require_once 'categorie_ingredients.php';
/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:28
 */
class ingredients
{
    private $id;
    private $categorie_ingredients;
    private $nom;
    private $unite;

    /**
     * ingredients constructor.
     * @param $id int
     * @param $categorie_ingredients categorie_ingredients
     * @param $nom string
     * @param $unite string
     */
    public function __construct($id, $categorie_ingredients, $nom, $unite)
    {
        $this->id = $id;
        $this->categorie_ingredients = categorie_ingredients::getById($categorie_ingredients);
        $this->nom = $nom;
        $this->unite = $unite;
    }

    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM ingredients WHERE INGREDIENTS_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new ingredients($res['INGREDIENTS_id'],
                $res['INGREDIENTS_categorie_ingredients_id'],
                $res['INGREDIENTS_nom'],
                $res['INGREDIENTS_unite']);
        else
            return false;
    }

    public static function getListeByCat($cat)
    {
        $reqGetAll = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM ingredients where INGREDIENTS_categorie_ingredients_id = :idCat ORDER  BY INGREDIENTS_nom;');
        $reqGetAll->execute(array(':idCat' => $cat));
        $resultats = $reqGetAll->fetchAll();
        foreach ($resultats as $res) {
            $ingredients[] = new ingredients($res['INGREDIENTS_id'],
                $res['INGREDIENTS_categorie_ingredients_id'],
                $res['INGREDIENTS_nom'],
                $res['INGREDIENTS_unite']);
        }
        return $ingredients;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return categorie_ingredients
     */
    public function getCategorieIngredients()
    {
        return $this->categorie_ingredients;
    }

    /**
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }
}
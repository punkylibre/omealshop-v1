<?php
require_once 'personne.php';
require_once 'foyer.php';
require_once 'produit.php';

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:11
 */
class produits_listes
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var bool|produit
     */
    private $produit;
    /**
     * @var bool|foyer
     */
    private $foyer;
    /**
     * @var bool|personne
     */
    private $demandeur;
    /**
     * @var int
     */
    private $quantite;
    /**
     * @var string
     */
    private $commentaire;

    /**
     * produits_listes constructor.
     * @param $id
     * @param $produit
     * @param $foyer
     * @param $demandeur
     * @param $quantite
     */
    public function __construct($id, $produit, $foyer, $demandeur, $quantite, $commentaire)
    {
        $this->id = $id;
        $this->produit = produit::getById($produit);
        $this->foyer = foyer::getById($foyer);
        $this->demandeur = personne::getById($demandeur);
        $this->quantite = $quantite;
        $this->commentaire = $commentaire;
    }

    /**
     * @param $id
     * @return bool|produits_listes
     */
    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM produits_listes WHERE PRODUITS_LISTES_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new produits_listes($res['PRODUITS_LISTES_id'],
                $res['PRODUITS_LISTES_produits_id'],
                $res['PRODUITS_LISTES_foyer_id'],
                $res['PRODUITS_LISTES_demandeur'],
                $res['PRODUITS_LISTES_quantite'],
                $res['PRODUITS_LISTES_commentaire']);
        else
            return false;
    }

    /**
     * @param $produit
     * @param $foyer
     * @param $personne
     * @param $quantite
     * @return bool
     */
    public static function addProduitToListe($produit, $foyer, $personne, $quantite, $commentaire)
    {
        $reqInsertProduit = PDO_OMealShop::connexionBDD()->prepare("INSERT INTO produits_listes(PRODUITS_LISTES_produits_id,
                    PRODUITS_LISTES_foyer_id,
                    PRODUITS_LISTES_demandeur,
                    PRODUITS_LISTES_quantite,
                    PRODUITS_LISTES_commentaire) 
                    VALUES(:idProduit, :idFoyer, :idPersonne, :quantite, :commentaire);");
        $reqInsertProduit->execute(array(':idProduit' => $produit->getId(), ':idFoyer' => $foyer->getId(), ':idPersonne' => $personne->getId(), ':quantite' => $quantite, ':commentaire' => $commentaire));
        if($reqInsertProduit->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @param $foyer
     * @return array
     */
    public static function getByFoyer($foyer)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM produits_listes WHERE PRODUITS_LISTES_foyer_id = :id');
        $reqGetById->execute(array(':id' => $foyer->getId()));
        $resultats = $reqGetById->fetchAll();
        foreach ($resultats as $res) {
            $liste[] = produits_listes::getById($res['PRODUITS_LISTES_id']);
        }
        return $liste;
    }

    /**
     * @param $idProduit
     * @return bool
     */
    public static function supprByIdProduit($idProduit)
    {
        $reqDeleteProduit = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM produits_listes WHERE PRODUITS_LISTES_produits_id = :id;");
        $reqDeleteProduit->execute(array(':id' => $idProduit));
        if($reqDeleteProduit->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @return bool
     */
    public function suppr()
    {
        $reqDeleteProduit = PDO_OMealShop::connexionBDD()->prepare("DELETE FROM produits_listes WHERE PRODUITS_LISTES_id = :id;");
        $reqDeleteProduit->execute(array(':id' => $this->id));
        if($reqDeleteProduit->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * @return bool|personne
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }

    /**
     * @return bool|produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }


}
<?php

/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 20:01
 */
class categorie_produits
{
    private $id;
    private $intitule;

    /**
     * categorie_produits constructor.
     * @param $id int
     * @param $intitule string
     */
    public function __construct($id, $intitule)
    {
        $this->id = $id;
        $this->intitule = $intitule;
    }

    public static function getById($id){
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_produits WHERE CATEGORIE_PRODUITS_id = :id');
        $reqGetById->execute(array(':id' => $id));
        if($res = $reqGetById->fetch())
            return new categorie_produits($res['CATEGORIE_PRODUITS_id'],
                $res['CATEGORIE_PRODUITS_intitule']);
        else
            return false;
    }

    public static function getListe()
    {
        $reqGetAll = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM categorie_produits;');
        $reqGetAll->execute(array());
        $resultats = $reqGetAll->fetchAll();
        foreach ($resultats as $res) {
            $categories[] = new categorie_produits($res['CATEGORIE_PRODUITS_id'],
                $res['CATEGORIE_PRODUITS_intitule']);
        }
        return $categories;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

}
<?php
session_start();
/**
 * Created by PhpStorm.
 * User: punk
 * Date: 23/06/17
 * Time: 19:57
 */

require_once 'DB.php';
require_once 'personne.php';
require_once 'foyer.php';
require_once 'membre_foyer.php';

class foyer
{
    private $id;
    private $nom;
    private $personne_createur;
    private $tempsSuppr;
    private $create_time;

    /**
     * foyer constructor.
     * @param $id int
     * @param $nom string
     * @param $personne_createur personne
     * @param $create_time DateTime
     */
    public function __construct($id, $nom, $personne_createur, $tempsSuppr, $create_time)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->personne_createur = personne::getById($personne_createur);
        $this->tempsSuppr = $tempsSuppr;
        $this->create_time = new DateTime($create_time);
    }


    public static function getById($id)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM foyer WHERE FOYER_id= :id');
        $reqGetById->execute(array(':id' => $id));
        if ($res = $reqGetById->fetch())
            return new foyer($res['FOYER_id'],
                $res['FOYER_nom'],
                $res['FOYER_personne_createur'],
                $res['FOYER_temps_suppr'],
                $res['FOYER_create_time']);
        else
            return false;
    }

    public static function getByNom($nom)
    {
        $reqGetById = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM foyer WHERE FOYER_nom = :nom');
        $reqGetById->execute(array(':nom' => $nom));
        if ($res = $reqGetById->fetch())
            return new foyer($res['FOYER_id'],
                $res['FOYER_nom'],
                $res['FOYER_personne_createur'],
                $res['FOYER_temps_suppr'],
                $res['FOYER_create_time']);
        else
            return false;
    }

    public static function addFoyer($nomFoyer, $personne)
    {
        $reqInsert = PDO_OMealShop::connexionBDD()->prepare('INSERT INTO foyer(FOYER_nom, FOYER_personne_createur, FOYER_create_time) VALUES(:nom, :idCreateur, NOW())');
        $reqInsert->execute(array(':nom' => $nomFoyer, ':idCreateur' => $personne->getId()));
        if ($reqInsert->rowCount() > 0) {
            $foyer = foyer::getByNom($nomFoyer);
            return membre_foyer::addMembre($foyer, $personne);
        }
        return false;
    }

    public static function existeDeja($nomFoyer)
    {
        $reqGetByNom = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM foyer WHERE FOYER_nom = :nomFoyer');
        $reqGetByNom->execute(array(':nomFoyer' => $nomFoyer));
        if ($res = $reqGetByNom->fetch())
            return true;
        else
            return false;
    }

    public static function getFoyersLike($search)
    {
        $session = unserialize($_SESSION['session']);
        $personne = $session->getPersonne();
        $reqGetBySearch = PDO_OMealShop::connexionBDD()->prepare('SELECT * FROM foyer WHERE FOYER_nom LIKE :search
                                                                  AND FOYER_id NOT IN (SELECT MEMBRE_FOYER_foyer_id FROM membre_foyer WHERE MEMBRE_FOYER_personne_id = :idPersonne);');
        $reqGetBySearch->execute(array(':search' => '%'.$search.'%', ':idPersonne' => $personne->getId()));
        $resultats = $reqGetBySearch->fetchAll();
        foreach ($resultats as $res) {
            $foyers[] = new foyer($res['FOYER_id'],
                $res['FOYER_nom'],
                $res['FOYER_personne_createur'],
                $res['FOYER_temps_suppr'],
                $res['FOYER_create_time']);
        }
        return $foyers;
    }

    public function suppression(){
        $retour = membre_foyer::supprFoyer($this->id);
        // a faire supprimer tout ce qui est en lien avec un foyer, liste de courses, repas plannifie ...

        if($retour){
            $reqDelete = PDO_OMealShop::connexionBDD()->prepare('DELETE FROM foyer WHERE FOYER_id = :idFoyer;');
            $reqDelete->execute(array(':idFoyer' => $this->id));
            if ($reqDelete->rowCount() > 0) {
                return true;
            }
            return false;
        }
    }

    public function changeModerateur($personne){
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE foyer SET FOYER_personne_createur = :idPersonne WHERE FOYER_id = :idFoyer;');
        $reqUpdate->execute(array(':idFoyer' => $this->id, ':idPersonne' => $personne->getId()));
        if ($reqUpdate->rowCount() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return bool|personne
     */
    public function getPersonneCreateur()
    {
        return $this->personne_createur;
    }

    /**
     * @return int
     */
    public function getTempsSuppr()
    {
        return $this->tempsSuppr;
    }

    public function setNom($nom){
        if($this->nom == $nom)
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE foyer SET FOYER_nom = :nom WHERE FOYER_id = :idFoyer;');
        $reqUpdate->execute(array(':idFoyer' => $this->id, ':nom' => $nom));
        if ($reqUpdate->rowCount() > 0) {
            $this->nom = $nom;
            return true;
        }
        return false;
    }

    /**
     * @param mixed $tempsSuppr
     */
    public function setTempsSuppr($tempsSuppr)
    {
        if($this->tempsSuppr == $tempsSuppr)
            return true;
        $reqUpdate = PDO_OMealShop::connexionBDD()->prepare('UPDATE foyer SET FOYER_temps_suppr = :tempsSuppr WHERE FOYER_id = :idFoyer;');
        $reqUpdate->execute(array(':idFoyer' => $this->id, ':tempsSuppr' => $tempsSuppr));
        if ($reqUpdate->rowCount() > 0) {
            $this->tempsSuppr = $tempsSuppr;
            return true;
        }
        return false;
    }
}
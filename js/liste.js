var joursSemaine = new Array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
var moi = new Array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre');

function genererListe(dateDSql, dateASql) {
    $.ajax({
        type: 'get',
        url: '../ctrl/genererListe.php?dateDeb=' + dateDSql + '&dateFin=' + dateASql
    }).done(function (data) {
            if (data.ok) {
                var tableau = [];
                var flag = false;

                for (var i in data.produits) {
                    var retour = jQuery.parseJSON(data.produits[i]);
                    if (i == 0 && !flag) {
                        flag = true;
                        tableau.push([]);
                        tableau[0].push(retour.categorieProduit);
                        tableau[0].push({
                            "type": "produit",
                            "idProduitListe": retour.idProduitListe,
                            "nomProduit": retour.nomProduit,
                            "quantite": retour.quantite,
                            "unite": retour.unite,
                            "commentaire": retour.commentaire
                        });
                    }
                    else {
                        loop:
                            for (var j = 0; j < tableau.length; ++j) {
                                if (tableau[j][0] == retour.categorieProduit) {
                                    for (var k = 1; k < tableau[j].length; ++k) {
                                        if (tableau[j][k].nomProduit == retour.nomProduit && retour.commentaire == tableau[j][k].commentaire) {
                                            tableau[j][k].quantite += retour.quantite;
                                            break loop;
                                        }
                                        else if (k == tableau[j].length - 1) {
                                            tableau[j].push({
                                                "type": "produit",
                                                "idProduitListe": retour.idProduitListe,
                                                "nomProduit": retour.nomProduit,
                                                "quantite": retour.quantite,
                                                "unite": retour.unite,
                                                "commentaire": retour.commentaire
                                            });
                                            break loop;
                                        }
                                    }
                                }
                                else if (j == tableau.length - 1) {
                                    tableau.push([]);
                                    tableau[tableau.length - 1].push(retour.categorieProduit);
                                    tableau[tableau.length - 1].push({
                                        "type": "produit",
                                        "idProduitListe": retour.idProduitListe,
                                        "nomProduit": retour.nomProduit,
                                        "quantite": retour.quantite,
                                        "unite": retour.unite,
                                        "commentaire": retour.commentaire
                                    });
                                    break loop;
                                }
                            }
                    }

                }

                for (var i in data.ingredients) {
                    var retour = jQuery.parseJSON(data.ingredients[i]);
                    if (i == 0 && !flag) {
                        flag = true;
                        tableau.push([]);
                        tableau[0].push(retour.categorieIngredient);
                        tableau[0].push({
                            "type": "ingredient",
                            "idIngredientListe": retour.idIngredientListe,
                            "nomIngredient": retour.nomIngredient,
                            "quantite": retour.quantite,
                            "unite": retour.unite,
                            "commentaire": retour.commentaire
                        });
                    }
                    else {
                        loop:
                            for (var j = 0; j < tableau.length; ++j) {
                                if (tableau[j][0] == retour.categorieIngredient) {
                                    for (var k = 1; k < tableau[j].length; ++k) {
                                        if (tableau[j][k].nomIngredient == retour.nomIngredient && retour.commentaire == tableau[j][k].commentaire) {
                                            tableau[j][k].quantite += retour.quantite;
                                            break loop;
                                        }
                                        else if (k == tableau[j].length - 1) {
                                            tableau[j].push({
                                                "type": "ingredient",
                                                "idIngredientListe": retour.idIngredientListe,
                                                "nomIngredient": retour.nomIngredient,
                                                "quantite": retour.quantite,
                                                "unite": retour.unite,
                                                "commentaire": retour.commentaire
                                            });
                                            break loop;
                                        }
                                    }
                                }
                                else if (j == tableau.length - 1) {
                                    tableau.push([]);
                                    tableau[tableau.length - 1].push(retour.categorieIngredient);
                                    tableau[tableau.length - 1].push({
                                        "type": "ingredient",
                                        "idIngredientListe": retour.idIngredientListe,
                                        "nomIngredient": retour.nomIngredient,
                                        "quantite": retour.quantite,
                                        "unite": retour.unite,
                                        "commentaire": retour.commentaire
                                    });
                                    break loop;
                                }
                            }
                    }
                }

                for (var i in data.ingredientsRepas) {
                    var retour = jQuery.parseJSON(data.ingredientsRepas[i]);
                    if (i == 0 && !flag) {
                        tableau.push([]);
                        tableau[0].push(retour.categorieIngredient);
                        tableau[0].push({
                            "type": "ingredientRepas",
                            "idIngredient": retour.idIngredient,
                            "nomIngredient": retour.nomIngredient,
                            "quantite": retour.quantite,
                            "unite": retour.unite
                        });
                    }
                    else {
                        loop:
                            for (var j = 0; j < tableau.length; ++j) {
                                if (tableau[j][0] == retour.categorieIngredient) {
                                    for (var k = 1; k < tableau[j].length; ++k) {
                                        if (tableau[j][k].nomIngredient == retour.nomIngredient && retour.commentaire == tableau[j][k].commentaire) {
                                            tableau[j][k].quantite += retour.quantite;
                                            break loop;
                                        }
                                        else if (k == tableau[j].length - 1) {
                                            tableau[j].push({
                                                "type": "ingredientRepas",
                                                "idIngredient": retour.idIngredient,
                                                "nomIngredient": retour.nomIngredient,
                                                "quantite": retour.quantite,
                                                "unite": retour.unite
                                            });
                                            break loop;
                                        }
                                    }
                                }
                                else if (j == tableau.length - 1) {
                                    tableau.push([]);
                                    tableau[tableau.length - 1].push(retour.categorieIngredient);
                                    tableau[tableau.length - 1].push({
                                        "type": "ingredientRepas",
                                        "idIngredient": retour.idIngredient,
                                        "nomIngredient": retour.nomIngredient,
                                        "quantite": retour.quantite,
                                        "unite": retour.unite
                                    });
                                    break;
                                }
                            }
                    }

                }

                var ul;
                var li;
                var listeCourses = $('#listeCourses');

                // console.table(tableau);

                for (var i = 0; i < tableau.length; ++i) {
                    categorie = $('<h3 />');
                    categorie.html(tableau[i][0]);
                    listeCourses.append(categorie);
                    ul = $('<ul class="list-group" />');
                    for (var j = 1; j < tableau[i].length; ++j) {
                        if (tableau[i][j].type == 'produit')
                            li = '<li class="list-group-item"> <button value="' + tableau[i][j].idProduitListe + '" class="produit btn btn-sm btn-link" type ="button"> ' + tableau[i][j].nomProduit + ' - ' + tableau[i][j].quantite + ' ' + tableau[i][j].unite +(tableau[i][j].commentaire!=''?' (' + tableau[i][j].commentaire + ')':'')+'</button> </li>';
                        else if (tableau[i][j].type == 'ingredientRepas')
                            li = '<li class="list-group-item"> <button value="' + tableau[i][j].idIngredient + '" class="ingredient btn btn-sm btn-link" type ="button"> ' + tableau[i][j].nomIngredient + ' - ' + tableau[i][j].quantite + ' ' + tableau[i][j].unite + '</button> </li>';
                        else if (tableau[i][j].type == 'ingredient')
                            li = '<li class="list-group-item"> <button value="' + tableau[i][j].idIngredientListe + '" class="ingredient btn btn-sm btn-link" type ="button"> ' + tableau[i][j].nomIngredient + ' - ' + tableau[i][j].quantite + ' ' + tableau[i][j].unite +(tableau[i][j].commentaire!=''?' (' + tableau[i][j].commentaire + ')':'')+'</button> </li>';
                        ul.append(li);
                    }
                    listeCourses.append(ul);
                }

                $('.ingredient').click(function () {
                    var idIngredient = $(this).val();
                    $(this).parent('li').remove();

                    $.ajax({
                        type: $(this).attr('method'),
                        url: '../ctrl/produits.php?action=supprIngredient&idIngredient='+idIngredient
                    }).done(function (data) {
                        if(data.ok) {
                            // location.href = '../index.php';
                        }
                    }).fail(erreurCritique);
                });

                $('.produit').click(function () {
                    var idProduit = $(this).val();
                    $(this).parent('li').remove();

                    $.ajax({
                        type: $(this).attr('method'),
                        url: '../ctrl/produits.php?action=supprProduit&idProduit='+idProduit
                    }).done(function (data) {
                        if(data.ok) {
                            // location.href = '../index.php';
                        }
                    }).fail(erreurCritique);
                });

            }
        }
    ).fail(erreurCritique);

}

$(document).ready(function () {
    var selectJour = $('.selectJour');
    for (var i = 1; i < 32; ++i) {
        option = new Option(i, i);
        selectJour.append(option);
    }

    var dateJour = new Date();
    $('#selectJourD').val(dateJour.getDate());
    if (dateJour.getMonth() < 10)
        $('#selectMoisD').val('0' + (parseInt(dateJour.getMonth(), 10) + 1));
    else
        $('#selectMoisD').val(parseInt(dateJour.getMonth(), 10) + 1);
    $('#selectAnneeD').val(dateJour.getFullYear());
    if (dateJour.getHours() < 8)
        $('#selectMomentD').val(8);
    else if (dateJour.getHours() < 12)
        $('#selectMomentD').val(12);
    else
        $('#selectMomentD').val(19);

    $('#selectJourA').val(parseInt(dateJour.getDate(), 10) + 1);
    if (dateJour.getMonth() < 10)
        $('#selectMoisA').val('0' + (parseInt(dateJour.getMonth(), 10) + 1));
    else
        $('#selectMoisA').val(parseInt(dateJour.getMonth(), 10) + 1);
    $('#selectAnneeA').val(dateJour.getFullYear());
    if (dateJour.getHours() < 8)
        $('#selectMomentA').val(8);
    else if (dateJour.getHours() < 12)
        $('#selectMomentA').val(12);
    else
        $('#selectMomentA').val(19);

    $('#valider').click(function () {
        var jourD = $('#selectJourD').val();
        var moisD = $('#selectMoisD').val();
        var anneeD = $('#selectAnneeD').val();
        var momentD = $('#selectMomentD').val();
        var jourA = $('#selectJourA').val();
        var moisA = $('#selectMoisA').val();
        var anneeA = parseInt($('#selectAnneeA').val(), 10);
        var momentA = parseInt($('#selectMomentA').val(), 10) + 1;
        $('#erreurFormat').hide();
        $('#erreurDate').hide();
        $('#succes').hide();


        if (moisD % 2 == 0 && jourD == 31) {
            $('#erreurFormat').show();
            return;
        }
        if (moisD == 2 && jourD == 29 && anneeD % 4 != 0) {
            $('#erreurFormat').show();
            return;
        }
        if (moisD == 2 && jourD > 29) {
            $('#erreurFormat').show();
            return;
        }
        if (moisA % 2 == 0 && jourA == 31) {
            $('#erreurFormat').show();
            return;
        }
        if (moisA == 2 && jourA == 29 && anneeA % 4 != 0) {
            $('#erreurFormat').show();
            return;
        }
        if (moisA == 2 && jourA > 29) {
            $('#erreurFormat').show();
            return;
        }


        var dateD = new Date(anneeD, moisD - 1, jourD, momentD);
        var dateA = new Date(anneeA, moisA - 1, jourA, momentA);
        var dateCourante = new Date();
        if (dateCourante.getTime() > dateD.getTime() || dateD.getTime() > dateA.getTime()) {
            $('#erreurDate').show();
            return;
        }

        $('#succes').show();
        var dateASql = dateA.getFullYear() + '-' + (dateA.getMonth() + 1) + '-' + dateA.getDate() + ' ' + momentA + ':0:0';
        var dateDSql = dateD.getFullYear() + '-' + (dateD.getMonth() + 1) + '-' + dateD.getDate() + ' ' + momentD + ':0:0';
        var jourDSelect = joursSemaine[dateD.getDay()] + ' ' + dateD.getDate().toString() + ' ' + moi[dateD.getMonth()] + ' ' + dateD.getFullYear().toString();
        var jourASelect = joursSemaine[dateA.getDay()] + ' ' + dateA.getDate().toString() + ' ' + moi[dateA.getMonth()] + ' ' + dateA.getFullYear().toString();

        $('#dateDebut').html(jourDSelect);
        $('#dateFin').html(jourASelect);

        $('#listeCourses').empty();

        genererListe(dateDSql, dateASql);
    });
});
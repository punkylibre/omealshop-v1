$(document).ready(function () {
    $("#btn-modif-info").click(function () {
        var formElement = $('#modifInfos').get(0);
        var formData = new FormData(formElement);

        $('.mailExiste').hide();
        $('.mailFormat').hide();
        $('.succesModif').hide();
        $.ajax({
            type: 'post',
            url: '../ctrl/parametre.php?action=modifInfos',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (!data.ok) {
                if (data.erreur == 1)
                    $('.mailExiste').show();
                else if(data.erreur == 2)
                    $('.mailFormat').show();
            }
            else
                $('.succesModif').show();
        });
    })

    $('#btn-modif-foyer').click(function () {
        var id = $('#liste-foyers').val();

        $('.succesFoyer').hide();
        $('.erreurFoyer').hide();
        $.ajax({
            type: 'get',
            url: '../ctrl/parametre.php?action=modifFoyer&id=' + id
        }).done(function (data) {
            if (data.ok) {
                $('.succesFoyer').show();
            }
            else {
                $('.erreurFoyer').show();
            }
        });
    })

    $("#btn-modif-mdp").click(function () {
        var formElement = $('#modifMDP').get(0);
        var formData = new FormData(formElement);

        $('.mdpFaux').hide();
        $('.mdpDifferent').hide();
        $('.mdpFormat').hide();
        $('.succesMDP').hide();
        $.ajax({
            type: 'post',
            url: '../ctrl/parametre.php?action=modifMDP',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data.ok == 1)
                $('.mdpFaux').show();
            if (data.ok == 2)
                $('.mdpDifferent').show();
            if (data.ok == 3)
                $('.mdpFormat').show();
            if (data.ok == 0)
                $('.succesMDP').show();
        });
    })

    $('.btn-suppr-compte').click(function () {
        $('.suppr-personne-erreur').hide();
        $('.suppr-personne-impossible').hide();
        var rep = confirm('Vous êtes sur le point de supprimer votre compte, voulez vous confirmer ?');
        if(rep){
            $.ajax({
                type: 'get',
                url: '../ctrl/parametre.php?action=supprimerCompte'
            }).done(function (data) {
                if (data.ok) {
                    $.ajax({
                        type: $(this).attr('method'),
                        url: '../ctrl/deconnexion.php'
                    }).done(function (data) {
                        if(data.ok) {
                            location.href = '../index.php';
                        }
                    }).fail(erreurCritique);
                    location.href = '../index.php';
                }
                else {
                    if(data.erreur == 1)
                        $('.suppr-personne-impossible').show();
                    if(data.erreur == 2)
                        $('.suppr-personne-erreur').show();
                }
            });
        }
    })
})
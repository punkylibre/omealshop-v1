/**
 * Created by punk on 31/07/17.
 */
var nbIngredients = 0;
function addIngredientDiv(idIngredient, idCategorie, unite, quantite) {
    var divIngredients = $('#div-ingredients');
    var newDiv = $('<div class="ingredient" />');
    var selectCat = $('<select class="form-control select-categorie" name="categorie" data-id="' + nbIngredients + '"/>');

    $.ajax({
        type: 'get',
        url: '../ctrl/gestionRepas.php?action=getCategories'
    }).done(function (data) {
        if (data.ok) {
            for (var i in data.categories) {
                var retour = jQuery.parseJSON(data.categories[i]);
                selectCat.append('<option value="' + retour.idCat + '">' + retour.nomCat + '</option>')
                selectCat.val(idCategorie);
            }
        }
    });

    selectCat.change(function () {
        var selectCategorie = $(this);
        var selectIngredient;
        $('.select-ingredients').each(function () {
            if ($(this).data('id') == selectCategorie.data('id')) {
                selectIngredient = $(this);
            }
        })

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionRepas.php?action=getIngredient&id=' + selectCategorie.val()
        }).done(function (data) {
            if (data.ok) {
                selectIngredient.empty();
                for (var i in data.ingredients) {
                    var retour = jQuery.parseJSON(data.ingredients[i]);
                    selectIngredient.append('<option value="' + retour.idIng + '">' + retour.nomIng + '</option>')
                }
                $.ajax({
                    type: 'get',
                    url: '../ctrl/gestionRepas.php?action=getUnite&id=' + selectIngredient.val()
                }).done(function (data) {
                    if (data.ok) {
                        punite.empty().html(data.unite);
                    }
                });
            }
        });

    })
    newDiv.append(selectCat);

    var selectIng = $('<select class="form-control select-ingredients" name="ingredient" data-id="' + nbIngredients + '"/>');
    $.ajax({
        type: 'get',
        url: '../ctrl/gestionRepas.php?action=getIngredient&id=' + idCategorie
    }).done(function (data) {
        if (data.ok) {
            for (var i in data.ingredients) {
                var retour = jQuery.parseJSON(data.ingredients[i]);
                selectIng.append('<option value="' + retour.idIng + '">' + retour.nomIng + '</option>')
                selectIng.val(idIngredient);
            }
        }
    });
    newDiv.append(selectIng);

    newDiv.append('<input name="quantite" value="' + quantite + '" class="form-control" placeholder="qt">');
    var punite = $('<p>' + unite + '</p>');
    selectIng.change(function () {
        var selectIngredient = $(this);
        $.ajax({
            type: 'get',
            url: '../ctrl/gestionRepas.php?action=getUnite&id=' + selectIngredient.val()
        }).done(function (data) {
            if (data.ok) {
                punite.empty().html(data.unite);
            }
        });

    })
    newDiv.append(punite);
    var btnSuppr = $('<button type="button" class="btn btn-danger btn-supprimer-ingredient">Supprimer</button>');
    btnSuppr.click(function () {
        $(this).parent().remove();
    })
    newDiv.append(btnSuppr);

    nbIngredients++;
    divIngredients.append(newDiv);
}

function chargerListeRepas(cat) {
    $.ajax({
        type: 'get',
        url: '../ctrl/gestionRepas.php?action=getRepas&cat=' + cat
    }).done(function (data) {
        var tbl = $('#liste-repas').empty();
        if (data.ok) {
            for (var i in data.listeRepas) {
                var retour = jQuery.parseJSON(data.listeRepas[i]);
                tbl.append('<tr><td>' + retour.nomRepas + '</td><td>' + retour.typeRepas + '</td><td>' + retour.categorieRepas + '</td><td>' + ((retour.createur == null) ? 'non' : 'oui') + '</td>'
                    + '<td><button value="' + retour.idRepas + '" type="button" class="btn btn-warning btn-editer">Editer</button></td></tr>');
            }
        }

        $('.btn-editer').click(function () {
            var id = $(this).val();
            $('#modif-repas').show();
            $('#div-ingredients').empty();
            $('#idRepas').val($(this).val());
            $.ajax({
                type: 'get',
                url: '../ctrl/gestionRepas.php?action=getIngredients&id=' + id
            }).done(function (data) {
                if (data.ok) {
                    $('#nom-repas').val(data.nomRepas);
                    $('#lst-categorie-repas-edition').val(data.idCategorie);
                    $('#type-repas').val(data.type);
                    if (data.supprimable) {
                        $('#btn-suppr-repas').show();
                        $('#btn-suppr-repas').val(id);
                    }
                    else
                        $('#btn-suppr-repas').hide();
                    for (var i in data.ingredients) {
                        var retour = jQuery.parseJSON(data.ingredients[i]);
                        addIngredientDiv(retour.idIngredient, retour.idCategorie, retour.unite, retour.quantite);
                    }
                }
            });
        })
    });
}

$(document).ready(function () {
    chargerListeRepas('tout');

    $('#lst-categorie-repas').change(function () {
        chargerListeRepas($('#lst-categorie-repas').val());
    })

    $('#btn-add-ingredient').click(function () {
        addIngredientDiv(0, 0, 'unite', '0');
    })

    $('#btn-add-repas').click(function () {
        $('#modif-repas').show();
        $('#div-ingredients').empty();
        $('#idRepas').val('new');
        $('#nom-repas').val("");
        $('#lst-categorie-repas-edition').val(0);
        $('#type-repas').val(0);
    })

    $('#btn-edit-repas').click(function () {
        $('.nomExiste').hide();
        $('.erreurAjoutIngredient').hide();
        $('.champsObligatoire').hide();

        var formElement = $('#edition-repas').get(0);
        var formData = new FormData(formElement);

        $("select[name=ingredient]").each(function () {
            formData.append('ingredient[]', $(this).val());
        });

        $("input[name=quantite]").each(function () {
            formData.append('quantite[]', $(this).val());
        });

        $.ajax({
            type: 'post',
            url: '../ctrl/gestionRepas.php?action=editRepas',
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data.ok) {
                $('#modif-repas').hide();
                chargerListeRepas('tout');
            }
            else {
                if (data.erreur == 1)
                    $('.nomExiste').show();
                if (data.erreur == 2)
                    $('.erreurAjoutIngredient').show();
                if (data.erreur == 3) {
                    $('.champsObligatoire').show();
                }
            }
        });
    })

    $('#btn-suppr-repas').click(function () {
        var id = $(this).val();
        $.ajax({
            type: 'post',
            url: '../ctrl/gestionRepas.php?action=supprRepas&id=' + id
        }).done(function (data) {
            if (data.ok) {
                $('#modif-repas').hide();
                $('.btn-editer').each(function () {
                    var idBtn = $(this).val();
                    if(idBtn == id){
                        $(this).parents('tr').remove();
                    }
                })
            }
        });
    })

})
function chargerListe(){
    $.ajax({
        type: 'get',
        url: '../ctrl/produits.php?action=getListeProduit'
    }).done(function (data) {
        var tbody = $('#tbl-liste-produit').empty();
        for (var i in data.liste) {
            var retour = jQuery.parseJSON(data.liste[i]);
            var tr = '<tr id="produit'+retour.idProduit+'"><td>'+retour.nomProduit+'</td><td>'+retour.catProduit+'</td><td>'+retour.quantite+'</td><td>'+
                retour.commentaire+'</td><td>'+retour.prenomDemandeur+' '+retour.nomDemandeur+
                '</td><td><button type="button" class="supprimerProduit btn btn-sm btn-danger" value="' + retour.idProduit + '">Supprimer</button></td></tr>';
            tbody.append(tr);
        }

        $('.supprimerProduit').click(function () {
            var btn = $(this);
            $.ajax({
                type: 'get',
                url: '../ctrl/produits.php?action=supprProduit&id=' + $(this).val()
            }).done(function (data) {
                if (data.ok) {
                    btn.parents('tr').remove();
                    $('#erreurSupprSansDate').hide();
                }
                else {
                    $('#erreursuppr').show();
                }
            }).fail(erreurCritique);

        });
    }).fail(erreurCritique);

    $.ajax({
        type: 'get',
        url: '../ctrl/produits.php?action=getListeIngredient'
    }).done(function (data) {
        var tbody = $('#tbl-liste-ingredient').empty();
        for (var i in data.liste) {
            var retour = jQuery.parseJSON(data.liste[i]);
            var tr = '<tr id="ingredient'+retour.idProduit+'"><td>'+retour.nomProduit+'</td><td>'+retour.catProduit+'</td><td>'+retour.quantite+'</td><td>'+
                retour.commentaire+'</td><td>'+retour.prenomDemandeur+' '+retour.nomDemandeur+
                '</td><td><button type="button" class="supprimerIngredient btn btn-sm btn-danger" value="' + retour.idProduit + '">Supprimer</button></td></tr>';
            tbody.append(tr);
        }

        $('.supprimerIngredient').click(function () {
            var btn = $(this);
            $.ajax({
                type: 'get',
                url: '../ctrl/produits.php?action=supprIngredient&id=' + $(this).val()
            }).done(function (data) {
                if (data.ok) {
                    btn.parents('tr').remove();
                    $('#erreurSupprSansDate').hide();
                }
                else {
                    $('#erreursuppr').show();
                }
            }).fail(erreurCritique);
        })
    }).fail(erreurCritique);

}

$(document).ready(function () {
    $.ajax({
        type: 'get',
        url: '../ctrl/estConnecte.php'
    }).done(function (data) {
        if (data) {
            $("#connecte").show();
            $("#navigationBarre").show();
            $("#racourcisIndex").show();
        }
        else {
            document.location.href = "http://omealshop.alwaysdata.net/index.php"
        }
    }).fail(erreurCritique);

    chargerListe();


    $('#bouton-alimentaire').click(function () {
        $('#ingredients').show();
        $('#produit').hide();
        $('#liste-ingredient').show();
        $('#liste-produits').hide();
    });

    $('#bouton-non-alimentaire').click(function () {
        $('#ingredients').hide();
        $('#produit').show();
        $('#liste-ingredient').hide();
        $('#liste-produits').show();
    });

    $('#bouton-alimentaire-liste').click(function () {
        $('#liste-ingredient').show();
        $('#liste-produits').hide();
        $('#ingredients').show();
        $('#produit').hide();
    });

    $('#bouton-non-alimentaire-liste').click(function () {
        $('#liste-ingredient').hide();
        $('#liste-produits').show();
        $('#ingredients').hide();
        $('#produit').show();
    });

    $('#succes').hide();
    $('#erreur').hide();

    $('#cat-produit').change(function () {
        var idCat = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/produits.php?action=getProduits&cat='+idCat
        }).done(function (data) {
            var select = $('#sel-produits').empty();
            for (var i in data.produits) {
                var retour = jQuery.parseJSON(data.produits[i]);
                var option = '<option value="'+retour.idProduit+'">'+retour.nomProduit+'</option>';
                select.append(option);
            }
        }).fail(erreurCritique);
    })

    $('#cat-ingredient').change(function () {
        var idCat = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/produits.php?action=getIngredients&cat='+idCat
        }).done(function (data) {
            var select = $('#sel-ingredient').empty();
            for (var i in data.produits) {
                var retour = jQuery.parseJSON(data.produits[i]);
                var option = '<option value="'+retour.idProduit+'">'+retour.nomProduit+'</option>';
                select.append(option);
                $('#punite').empty();
            }
        }).fail(erreurCritique);
    })

    $('#sel-ingredient').change(function () {
        var idProd = $(this).val();

        $.ajax({
            type: 'get',
            url: '../ctrl/gestionRepas.php?action=getUnite&id=' + idProd
        }).done(function (data) {
            if (data.ok) {
                $('#punite').empty().html(data.unite);
            }
        });
    })

    $("#produit").submit(function () {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize()
        }).done(function (data) {
            if (data.ok) {
                $('#succes').show();
                $('#erreur').hide();
                chargerListe();
            }
            else {
                $('#erreur ul').empty();
                var ul = $('<ul />');
                for (var j in data.erreur) {
                    ul.append($('<li>').html(data.erreur[j]));
                }
                $('#succes').hide();
                $('#erreur').append(ul).show();
            }
        }).fail(erreurCritique);
        return false;  //a garder sinon va recharger la page
    });

    $("#ingredients").submit(function () {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize()
        }).done(function (data) {
            if (data.ok) {
                $('#succes').show();
                $('#erreur').hide();
                chargerListe();
            }
            else {
                $('#erreur ul').empty();
                var ul = $('<ul />');
                for (var j in data.erreur) {
                    ul.append($('<li>').html(data.erreur[j]));
                }
                $('#succes').hide();
                $('#erreur').append(ul).show();
            }
        }).fail(erreurCritique);
        return false;  //a garder sinon va recharger la page
    });

});

<?php
session_start();
require_once('../model/personne.php');
require_once('../model/session.php');
require_once('../model/foyer.php');
if (isset($_SESSION['session'])) {
    $session = unserialize($_SESSION['session']);
    $personne = $session->getPersonne();
    $foyer = $session->getFoyer();
    $foyers = membre_foyer::getFoyersPersonne($personne->getId());
}

require_once '../view/nav.php';
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="home.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li>
                <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
            </li>
            <li>
                <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
            </li>
            <li>
                <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li>
                <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <strong>Paramètres :</strong> Gestion des paramètres de votre compte
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active">
                            <i class="fa fa-wrench"></i> Paramètres
                        </li>
                    </ol>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Mes paramètres : </h3>
                        </div>
                        <div class="panel-body">
                            <form id="modifInfos">
                                <div class="alert alert-success succesModif" style="display:none;">
                                    <strong>Succès !</strong> Les modifications ont été prises en compte.
                                </div>
                            <label style="display: inline-block;">Mail : </label>
                            <input id="mail" class="form-control" name="mail" value="<?=$personne->getMail()?>"/>
                            <div class="alert alert-danger mailExiste" style="display:none;">
                                <strong>Erreur !</strong> Le mail que vous avez saisi est déjà utilisé.
                            </div>
                                <div class="alert alert-danger mailFormat" style="display:none;">
                                    <strong>Erreur !</strong> Le format du mail que vous avez saisi est invalide.
                                </div>

                            <label style="display: inline-block;">Nom : </label>
                            <input id="nom" class="form-control" name="nom" value="<?=$personne->getNom()?>"/>
                            <label style="display: inline-block;">Prenom : </label>
                            <input id="prenom" class="form-control" name="prenom" value="<?=$personne->getPrenom()?>"/> <br/>
                            <button id="btn-modif-info" type="button" class="btn btn-success">Valider</button>
                            </form>
                            <br /><br />
                            <label style="display: inline-block;">Foyer par défaut</label>
                            <div class="alert alert-success succesFoyer" style="display:none;">
                                <strong>Succès !</strong> Le foyer par défaut a été changé.
                            </div>
                            <div class="alert alert-danger erreurFoyer" style="display:none;">
                                <strong>Erreur !</strong> Il y a eu une erreur, veuillez essayer de nouveau, si le problème persite contacter l'administrateur.
                            </div>
                            <div class="input-group custom-search-form">
                                <select id="liste-foyers" class="form-control">
                                <?php
                                foreach ($foyers as $foyer){
                                    echo '<option value="'.$foyer->getId().'">'.$foyer->getNom().'</option>';
                                }
                                ?>
                                </select>
                                <span class="input-group-btn">
                        <button id="btn-modif-foyer" type="button" class="btn btn-success">
                            Valider
                        </button>
                            </span>
                            </div><br /><br />
                            <div class="alert alert-danger suppr-personne-erreur" style="display:none;">
                                <strong>Erreur !</strong> Le foyer n'a pu être supprimé.
                            </div>
                            <div class="alert alert-danger suppr-personne-impossible" style="display:none;">
                                <strong>Erreur !</strong> Vous êtes modérateur de un ou plusieurs foyer, veuillez d'abord changer le modérateur des foyers concernés ou les supprimer.
                            </div>
                            <button type="button" class="btn btn-danger btn-suppr-compte">Supprimer le compte
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Mot de passe : </h3>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-danger mdpFaux" style="display:none;">
                                <strong>Erreur !</strong> Mot de passe inconnu.
                            </div>
                            <div class="alert alert-danger mdpDifferent" style="display:none;">
                                <strong>Erreur !</strong> Les mots de passes ne correspondent pas.
                            </div>
                            <div class="alert alert-danger mdpFormat" style="display:none;">
                                <strong>Erreur !</strong> Le mot de passe que vous aviez saisi n'est pas correct.
                            </div>
                            <div class="alert alert-success succesMDP" style="display:none;">
                                <strong>Succès !</strong> Votre mot de passe a été changé.
                            </div>
                            <form id="modifMDP">
                            <label style="display: inline-block;">Ancien mot de passe : </label>
                            <input id="formerPWD" type="password" class="form-control" name="formerPWD" placeholder="********"/>
                            <label style="display: inline-block;">Nouveau mot de passe : </label>
                            <input id="pwd" type="password" class="form-control" name="pwd" placeholder="********"/>
                            <p class="help-block">Doit comporter une majuscule, un chiffre et un caractère spécial (tous sauf lettre et chiffre) et être d'une taille de 8 caractères minimum</p>
                            <label style="display: inline-block;">Confirmer le nouveau mot de passe : </label>
                            <input id="confirmPWD" type="password" class="form-control" name="confirmPWD" placeholder="********"/> <br/>
                            <button id="btn-modif-mdp" type="button" class="btn btn-success">Valider</button>
                            </form>
                            <br /><br />
                        </div>
                    </div>
                </div>
            </div>

            <!-- div row -->

        </div>
        <!-- /.container-fluid -->
        <script src="../js/parametres.js"></script>
        <script src="../js/main.js"></script>

<?php
require_once '../view/footer.php';
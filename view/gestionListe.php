<?php
require_once "nav.php";
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse" >
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li >
                <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li>
                <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
            </li>
            <li>
                <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
            </li>
            <li >
                <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="gestionListe.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <strong>Générer la liste de course :</strong> Générez la liste de course de votre foyer pour une période donnée
                    </h1>
                </div>
            </div>

            <!-- div row -->
            <div class="col-lg-6 text-center">
                <div class="panel panel-default">
                    <h1 class="page-header jourSelect">Créer la liste de courses</h1>
                    <div id="info" class="alert alert-info">
                        Veuillez séléctionner une date de départ et une date de fin pour générer votre liste de courses.
                    </div>
                    <div id="erreurFormat" class="alert alert-danger" style="display: none;">
                        L'une des dates sélectionnées n'est pas valide.
                    </div>
                    <div id="erreurDate" class="alert alert-danger" style="display: none;">
                        La date de départ est postérieure à celle de fin ou antérieure à la date courante.
                    </div>
                    <div id="succes" class="alert alert-success" style="display: none;">
                        Votre liste de courses a bien été générées.
                    </div>


                    <h3>Jour de début</h3>
                    <label for="selectJour">Jour</label>
                    <select name="selectJour" class="selectJour" id="selectJourD">

                    </select>

                    <label for="selectMois">Mois</label>
                    <select name="selectMois" id="selectMoisD">
                        <option value="01">Janvier</option>
                        <option value="02">Février</option>
                        <option value="03">Mars</option>
                        <option value="04">Avril</option>
                        <option value="05">Mai</option>
                        <option value="06">Juin</option>
                        <option value="07">Juillet</option>
                        <option value="08">Aout</option>
                        <option value="09">Septembre</option>
                        <option value="10">Octobre</option>
                        <option value="11">Novembre</option>
                        <option value="12">Décembre</option>
                    </select>

                    <label for="selectAnnee">Année</label>
                    <select name="selectAnnee"  id="selectAnneeD">
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                    <label for="selectMoment">Moment</label>
                    <select name="selectMoment"  id="selectMomentD">
                        <option value="8">Petit déjeuner</option>
                        <option value="12">Déjeuner</option>
                        <option value="19">Diner</option>
                    </select>

                    <h3>Jour de fin</h3>
                    <label for="selectJour">Jour</label>
                    <select name="selectJour" class="selectJour" id="selectJourA">

                    </select>

                    <label for="selectMois">Mois</label>
                    <select name="selectMois"  id="selectMoisA">
                        <option value="01">Janvier</option>
                        <option value="02">Février</option>
                        <option value="03">Mars</option>
                        <option value="04">Avril</option>
                        <option value="05">Mai</option>
                        <option value="06">Juin</option>
                        <option value="07">Juillet</option>
                        <option value="08">Aout</option>
                        <option value="09">Septembre</option>
                        <option value="10">Octobre</option>
                        <option value="11">Novembre</option>
                        <option value="12">Décembre</option>
                    </select>

                    <label for="selectAnnee">Année</label>
                    <select name="selectAnnee"  id="selectAnneeA">
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                    <label for="selectMoment">Moment</label>
                    <select name="selectMoment" id="selectMomentA">
                        <option value="8">Petit déjeuner</option>
                        <option value="12">Déjeuner</option>
                        <option value="19">Diner</option>
                    </select><br /><br />
                    <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                </div>
            </div>

            <div class="col-lg-6 text-center">
                <div class="panel panel-default">
                    <h1 class="page-header jourSelect">Votre liste de courses</h1>
                    <div id="dateCourses" class="alert alert-info">
                        Voici votre liste de courses du <span id="dateDebut"></span> au <span id="dateFin"></span>.
                    </div>
                    <div id="listeCourses">

                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->
        <script src="../js/liste.js"></script>
        <script src="../js/main.js"></script>

<?php
include_once 'footer.php';
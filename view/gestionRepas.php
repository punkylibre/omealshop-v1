<?php
require_once "../view/nav.php";
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li>
                <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li>
                <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
            </li>
            <li>
                <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
            </li>
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="gestionRepas.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li>
                <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <strong>Gestion des repas :</strong> Gestion des repas de votre compte
                </h1>
            </div>
        </div>

        <div class="col-lg-7 text-center">
            <div class="panel panel-default">
                <h3 class="page-header">Liste des repas :</h3>
                <select id="lst-categorie-repas" name="lst-categorie-repas">
                    <option value="tout">Toutes les catégories</option>
                    <?php
                    require_once '../model/categorie_repas.php';
                    $categoriesRepas = categorie_repas::getAll();
                    var_dump($categoriesRepas);
                    foreach ($categoriesRepas as $cat)
                        echo '<option value="' . $cat->getId() . '">' . $cat->getIntitule() . '</option>';
                    ?>
                </select>
                <button id="btn-add-repas" type="button" class="btn btn-success">Ajouter un nouveau repas</button>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom du repas</th>
                        <th>Type</th>
                        <th>Catégorie de repas</th>
                        <th>Recette du foyer</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="liste-repas">

                    </tbody>
                </table>
            </div>
        </div>

        <div id="modif-repas" class="col-lg-5 text-center" style="display: none">
            <div class="panel panel-default">
                <h3 class="page-header">Repas :
                    <p id="repas-selectionne">Sélectionner un repas </p></h3>
                <div class="alert alert-danger nomExiste" style="display:none;">
                    <strong>Erreur !</strong> Vous avez déjà créé un repas portant ce nom.
                </div>
                <div class="alert alert-danger erreurAjoutIngredient" style="display:none;">
                    <strong>Erreur !</strong> Un des ingrédients n'a pu être ajouté, veuillez éditer le repas que vous avez créé.
                </div>
                <div class="alert alert-danger champsObligatoire" style="display:none;">
                    <strong>Erreur !</strong> Les champs "nom du repas", "catégorire du repas" et "type de repas" sont obligatoires.
                </div>
                <form id="edition-repas">
                    <label>Nom du repas</label>
                    <input id="nom-repas" class="form-control" name="nom-repas">
                    <label>Catégorie du repas</label>
                    <select id="lst-categorie-repas-edition" class="form-control" name="lst-categorie-repas">
                        <?php
                        require_once '../model/categorie_repas.php';
                        $categoriesRepas = categorie_repas::getAll();
                        foreach ($categoriesRepas as $cat)
                            echo '<option value="' . $cat->getId() . '">' . $cat->getIntitule() . '</option>';
                        ?>
                    </select><br/>
                    <label>Type de repas</label>
                    <select id="type-repas" class="form-control" name="type-repas">
                        <option value="petit-dejeuner">Petit déjeuner</option>
                        <option value="entree">Entrée</option>
                        <option value="plat">Plat</option>
                        <option value="dessert">Dessert</option>
                    </select><br/>
                    <button id="btn-suppr-repas" type="button" class="btn btn-danger" style="display: none">Supprimer le repas</button><br />
                    <label>Liste des ingrédients</label>
                    <div id="div-ingredients">

                    </div>
                    <button id="btn-add-ingredient" type="button" class="btn btn-primary">Ajouter un ingrédient</button><br /> <br />
                    <input id="idRepas" type="hidden" name="idRepas"/>
                    <button id="btn-edit-repas" type="button" class="btn btn-success">Valider</button>
                </form>
            </div>
        </div>

        <!-- div row -->

    </div>
    <!-- /.container-fluid -->
    <script src="../js/main.js"></script>
    <script src="../js/gestionRepas.js"></script>

<?php
require_once '../view/footer.php';
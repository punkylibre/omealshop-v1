    <?php
    require_once "../view/nav.php";
    ?>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
                <li class="active" style="background-color: #1d1d1d !important;">
                    <a href="home.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                </li>
                <li>
                    <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
                </li>
                <li>
                    <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
                </li>
                <li>
                    <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
                </li>
                <li>
                    <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <strong>OMealShop :</strong> Organiser votre liste de course en fonction de vos repas
                    </h1>
                </div>
            </div>

            <div id="introNonConnecte" class="jumbotron" style="display: none;">
                <h1>Bienvenue sur OMealShop</h1>
                OMealShop est un site vous permettant de gérer vos listes de courses plus facilement directement en ligne en la partageant avec votre foyer.

                Vous pouvez planifier vos repas des semaines à venir en choisissant le nombre de personnes pour chaque repas et ainsi mieux gérer votre liste de courses.
                Les repas peuvent être choisis parmi la liste des repas déjà présent sur OMealShop (que vous pourrez éditer à votre convenance) ou parmi la liste des repas que vous aurez créé.

                Vous pouvez également ajouter des produits sur votre liste de courses comme par exemple les produits d'entretiens ou des ingrédients de façon individuelle.

                Lorsque vous partez faire vos courses vous n'avez plus qu'à sélectionner une date de début et une date de fin qui correspond à la période pour laquelle la liste
                de courses va être générée avec les repas planifiés. Vous avez alors votre liste de course avec tous les repas planifiés ainsi que tous les produits ou ingrédients que vous aurez pu ajouter.
                Lorsque vous les avez mis dans votre caddie il suffit de cliquer sur l'item et il disparaît de votre liste.

                OmealShop c’est déjà plus de 1600 produits alimentaire et plus de 500 produits non alimentaire référencés.</p>
                </p>
            </div>

            <?php
            if (isset($_SESSION['session'])) {
                $session = unserialize($_SESSION['session']);
                $personne = $session->getPersonne();
                $foyers = membre_foyer::getFoyersPersonne($personne->getId());
                $flag = false;
                foreach ($foyers as $foyer) {
                    if($foyer->getPersonneCreateur()->getId() == $personne->getId()) {
                        $demandes = membre_foyer::getDemandesMembres($foyer->getId());
                        if (sizeof($demandes) > 0) {
                            if(!$flag){
                                echo '<div class="alert alert-info">';
                            }
                            echo 'Vous avez des demandes pour rejoindre le foyer : '.$foyer->getNom().'<br />';
                            $flag = true;
                        }
                    }
                }
                if($flag)
                    echo 'Cliquez sur le lien pour accèder à la gestion des foyers directement <a href="../view/gestionFoyer.php">cliquez ici</a></div>';

            }
            ?>

            <div id="racourcisIndex" class="row" style="display: none;">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <?php
                                        require_once '../model/repas_plannifie.php';
                                        require_once '../model/session.php';
                                        require_once '../model/foyer.php';
                                        if (isset($_SESSION['session'])) {
                                            $session = unserialize($_SESSION['session']);
                                            $foyer = $session->getFoyer();
                                            $date = new DateTime('+3 month');
                                            if (isset($foyer)) {
                                                $repasPlannifies = repas_plannifie::getRepasBetweenDate(date('y-m-d'), date_format($date, 'y-m-d'), $foyer);
                                                $count = sizeof($repasPlannifies);
                                                $repasNonPlannifiés = repas_plannifie::getRepasBetweenDate('2000-01-01 00:00:00', '2000-01-01 00:05:00', $foyer);
                                                $count += sizeof($repasNonPlannifiés);
                                                echo $count;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div>Repas planifié</div>
                                </div>
                            </div>
                        </div>
                        <a href="planning.php">
                            <div class="panel-footer">
                                <span class="pull-left">Aller à "Plannifier des repas"</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <?php
                                        require_once '../model/ingredients_listes.php';
                                        require_once '../model/produits_listes.php';
                                        if (isset($_SESSION['session'])) {
                                            if (isset($foyer)) {
                                                $ingredientsListe = ingredients_listes::getByFoyer($foyer);
                                                $count = sizeof($ingredientsListe);
                                                $produitsListe = produits_listes::getByFoyer($foyer);
                                                $count += sizeof($produitsListe);
                                                echo $count;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div>Produits ajoutés</div>
                                </div>
                            </div>
                        </div>
                        <a href="produits.php">
                            <div class="panel-footer">
                                <span class="pull-left">Aller à "Ajouter un produit"</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                        <?php
                                        require_once '../model/repas.php';
                                        if (isset($_SESSION['session']))
                                            if (isset($foyer)) {
                                                echo sizeof(repas::getRepasByFoyer($foyer->getId()));
                                            }
                                        ?>
                                    </div>
                                    <div>Repas créés</div>
                                </div>
                            </div>
                        </div>
                        <a href="gestionRepas.php">
                            <div class="panel-footer">
                                <span class="pull-left">Aller à "Gestion des repas"</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- div row -->

        </div>
        <!-- /.container-fluid -->
        <script src="../js/index.js"></script>
        <script src="../js/main.js"></script>

    <?php
    require_once '../view/footer.php';


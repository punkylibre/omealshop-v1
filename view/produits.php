<?php
require_once "nav.php";
require_once '../model/categorie_produits.php';
require_once '../model/categorie_ingredients.php';
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li>
                <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li>
                <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
            </li>
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="produits.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-edit" style="color: #FF6600 !important;"></i> Ajouter un produit</a>
            </li>
            <li>
                <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li>
                <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <strong>Produits :</strong> Ajouter différents produits à la liste de courses
                </h1>
            </div>
        </div>

        <div id="produits" class="col-lg-4 text-center">
            <div class="panel panel-default">
                <h3 class="page-header jourSelect">Sélection des produits</h3>
                <button id="bouton-non-alimentaire" type="button" class="btn btn-info">Non alimentaire</button>
                <button id="bouton-alimentaire" type="button" class="btn btn-info">Alimentaire</button>
                <br/>

                <div id="succes" class="alert alert-success" style="display: none;">
                    <strong>Succès!</strong> Votre produit a bien été ajouté à la liste.
                </div>
                <div id="erreur" class="alert alert-danger" style="display: none;">
                    <strong>Erreur!</strong> Il y a eu une erreur votre produit n'a pu être ajouté à la liste.
                </div>

                <form id="produit" method="post" action="../ctrl/produits.php?action=nonAlimentaire"
                      role="form">
                    <div class="form-group">
                        <h3 class="page-header">Produits non alimentaires</h3>
                        <label>Catégorie du produit </label>
                        <select id="cat-produit" name="cat-produit" class="form-control">
                            <option value="aucun">Veuillez choisir une catégorie</option>
                            <?php
                            $categorieProduits = categorie_produits::getListe();
                            foreach ($categorieProduits as $cat) {
                                echo '<option value="' . $cat->getId() . '">' . $cat->getIntitule() . '</option>';
                            }
                            ?>
                        </select>
                        <label>Produits </label>
                        <select id="sel-produits" name="produit" class="form-control">
                            <option value="aucun">Veuillez choisir une catégorie</option>
                        </select><br/>
                        <label>Quantité</label>
                        <input type="number" min="1" class="form-control" value="1" name="quantite">
                        <label>Commentaire</label>
                        <input type="text" class="form-control" name="commentaire" placeholder="marque, modèle ...">
                        <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </form>

                <form id="ingredients" method="post" action="../ctrl/produits.php?action=alimentaire" role="form"
                      style="display : none;">
                    <div class="form-group">
                        <h3 class="page-header">Produits alimentaires</h3>
                        <label>Catégorie du produit </label>
                        <select id="cat-ingredient" name="cat-ingredient" class="form-control">
                            <option value="aucun">Veuillez choisir une catégorie</option>
                            <?php
                            $categorieIngredients = categorie_ingredients::getListe();
                            foreach ($categorieIngredients as $cat) {
                                echo '<option value="' . $cat->getId() . '">' . $cat->getIntitule() . '</option>';
                            }
                            ?>
                        </select>
                        <label>Produits </label>
                        <select id="sel-ingredient" name="ingredient" class="form-control">
                            <option value="aucun">Veuillez choisir une catégorie</option>
                        </select><br/>
                        <label>Quantité</label>
                        <input type="number" min="1" class="form-control" name="quantite"><p id="punite"></p>
                        <label>Commentaire</label>
                        <input type="text" class="form-control" name="commentaire" placeholder="marque, modèle ...">
                        <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="produits" class="col-lg-8 text-center">
            <div class="panel panel-default">
                <h3 class="page-header jourSelect">Liste actuelle</h3>
                <button id="bouton-non-alimentaire-liste" type="button" class="btn btn-info">Non alimentaire</button>
                <button id="bouton-alimentaire-liste" type="button" class="btn btn-info">Alimentaire</button>
                <div id="erreurSuppr" class="alert alert-danger" style="display: none;">
                    <strong>Erreur!</strong> Il y a eu une erreur le produit n'a pu être supprimé de la liste.
                </div>
                <table id="liste-produits" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nom du produit</th>
                        <th>Catégorie</th>
                        <th>Quantité</th>
                        <th>Commentaire</th>
                        <th>Demandeur</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody id="tbl-liste-produit"/>

                </table>
                <table id="liste-ingredient" class="table table-bordered table-hover" style="display: none">
                    <thead>
                    <tr>
                        <th>Nom du produit</th>
                        <th>Catégorie</th>
                        <th>Quantité</th>
                        <th>Commentaire</th>
                        <th>Demandeur</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody id="tbl-liste-ingredient"/>

                </table>
            </div>
        </div>

        <!-- div row -->

    </div>
    <!-- /.container-fluid -->
    <script src="../js/produits.js"></script>
    <script src="../js/main.js"></script>

<?php
require_once 'footer.php';
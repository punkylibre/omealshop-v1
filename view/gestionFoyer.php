<?php
require_once "../view/nav.php";
require_once '../model/session.php';
require_once '../model/membre_foyer.php';
require_once '../model/foyer.php';
require_once '../model/personne.php';

$session = unserialize($_SESSION['session']);
$personne = $session->getPersonne();
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="home.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li>
                <a href="planning.php"><i class="fa fa-fw fa-table"></i> Plannifier des repas</a>
            </li>
            <li>
                <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
            </li>
            <li>
                <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li>
                <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="well">
                <h3><strong>Gestion des foyers</strong></h3>
                <p>Vous pouvez ici gérer vos foyers, ceux dont vous êtes membres et ceux dont vous êtes le créateur ou
                    en rejoindre un nouveau.</p>
            </div>
        </div>

        <?php if(isset($_GET['erreurFoyer'])){
            echo '<div class="col-lg-12"><div class="alert alert-danger">
                    <strong>Pas de foyer par défaut!</strong> Veuillez sélectionner un foyer par défaut, en rejoindre un ou en créer un afin d\'accèder aux différentes fonctionnalités du site.
                </div></div> ';
        }
        ?>

        <div class="col-lg-12">
            <h3><strong>Mes foyers</strong></h3>
            <div id="erreurQuitter" class="alert alert-danger" style="display: none">
                <strong>Erreur</strong> Il y a eu une erreur pour quitter le foyer
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Foyer par défaut</th>
                    <th>Nom du foyer</th>
                    <th>Modérateur</th>
                    <th>Liste des membres</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $foyers = membre_foyer::getFoyersPersonne($personne->getId());
                $foyerDefaut = membre_foyer::getFoyerParDefaut($personne->getId());

                if (sizeof($foyers) > 0) {
                    foreach ($foyers as $foyer) {
                        if(isset($foyerDefaut))
                        echo '<tr><td><div class="radio">
                                    <label>
                                        <input name="foyer-default" class="foyer-default" value="'.$foyer->getId().'" '.($foyer->getId() == $foyerDefaut->getId()?'checked=""':'').' type="radio">
                                    </label>
                                </div></td>';
                        else
                            echo '<tr><td><div class="radio">
                                    <label>
                                        <input name="foyer-default" class="foyer-default" value="'.$foyer->getId().'" type="radio">
                                    </label>
                                </div></td>';
                        echo '<td>' . $foyer->getNom() . '</td>';
                        $createur = $foyer->getPersonneCreateur();
                        echo '<td>' . $createur->getPrenom() . ' ' . $createur->getNom() . '</td>';
                        $membres = membre_foyer::getMembresFoyer($foyer->getId());
                        echo '<td>';
                        foreach ($membres as $membre) {
                            if ($membre->getId() != $personne->getId()) {
                                echo $membre->getPrenom() . ' ' . $membre->getNom() . ' ';
                                if ($foyer->getPersonneCreateur()->getId() == $personne->getId())
                                    echo '<button type="button" class="btn btn-danger btn-suppr-membre" value="' . $membre->getId() . '" data-foyer="' . $foyer->getId() . '">Supprimer membre</button></br>';
                            }
                        }
                        echo '</td><td>';
                        if ($foyer->getPersonneCreateur()->getId() == $personne->getId())
                            echo '<button value="' . $foyer->getId() . '"type="button" class="btn btn-warning btn-moderer">Modérer</button>';
                        else
                            echo '<button value="' . $foyer->getId() . '"type="button" class="btn btn-danger btn-quitter">Quitter</button>';
                        echo '</td>';

                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Gestion du foyer : <p class="nom-foyer" style="display: inline">
                                Sélectionnez un foyer</p></h3>
                    </div>
                    <div class="panel-body">
                        <label class="foyer-selectionne" style="display: none;">Nom du foyer</label>
                        <div class="alert alert-danger nomExiste" style="display:none;">
                            <strong>Erreur !</strong> Le nom de foyer que vous avez saisi est déjà utilisé.
                        </div>
                        <div class="input-group custom-search-form">
                            <input id="nomFoyer" class="form-control foyer-selectionne" style="display:none;">
                            <span class="input-group-btn">
                            <button id="btn-modif-nom" type="button" class="btn btn-success foyer-selectionne"
                                    style="display:none;">Modifier le nom
                            </button>
                            </span>
                        </div>
                        <br/>
                        <div class="alert alert-danger suppr-foyer-impossible" style="display:none;">
                            <strong>Erreur !</strong> Le foyer n'a pu être supprimé.
                        </div>
                        <button type="button" class="btn btn-danger btn-suppr-foyer foyer-selectionne"
                                style="display: none">Supprimer le
                            foyer
                        </button>
                        <br/><br />
                        <label class="foyer-selectionne" style="display: none;">Changez le modérateur</label>
                        <div class="input-group custom-search-form">
                            <select id="liste-membres" class="form-control foyer-selectionne" style="display:none;">
                                <option value="not">Sélectionnez une personne</option>
                            </select>
                            <span class="input-group-btn">
                        <button id="btn-modif-moderateur" type="button" class="btn btn-success foyer-selectionne"
                                style="display:none;">
                            Valider
                        </button><br />
                            </span>
                        </div>
                        <label class="foyer-selectionne" style="display:none;">Temps de suppression des menus</label>
                        <div class="alert alert-danger modif-temps-impossible" style="display:none;">
                            <strong>Erreur !</strong> Le temps doit être compris entre 1 et 8 semaines.
                        </div>
                        <div class="input-group custom-search-form foyer-selectionne" style="display:none;">
                            <input id="tempsSuppr" name="tempsSuppr" type="number" min="1" max="8" class="form-control">
                            <span class="input-group-btn">
                        <button id="btn-modif-temps" type="button" class="btn btn-success foyer-selectionne"
                                style="display:none;">
                            Valider
                        </button>
                            </span>
                        </div>
                        <p class="help-block foyer-selectionne" style="display:none;">Ceci est le temps avant que les anciens repas plannifiés ne soit supprimés (compris entre 1 et 8 semaines).</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Gestion des membres du foyer : <p class="nom-foyer"
                                                                                  style="display: inline">Sélectionnez
                                un foyer</p></h3>
                    </div>
                    <div class="panel-body">
                        <div class="navbar navbar-default foyer-selectionne" style="display:none;">
                            <div class="container">
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li id="btn-add-personne" class="active"><a>Ajouter une personne</a>
                                        </li>
                                        <li id="btn-demandes"><a>Gérer les demandes</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--/.nav-collapse -->
                            </div>
                        </div>
                    </div>
                    <div id="add-personne" style="display: none" class="foyer-selectionne">
                        <i class="fa fa-search-plus fa-fw"></i>
                        Vous pouvez chercher un utilisateur à partir du champ de recherche.
                        <div class="input-group custom-search-form">
                            <input class="form-control " id="search" placeholder="Chercher un mail, nom ou un prenom">
                            <span class="input-group-btn">
                                <button id="btnSearch" class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <div class="alert alert-warning envoyer-mail" style="display:none;">
                            <strong>Membre non trouvé !</strong> Aucune personne n'a été trouvé correspondant à cette adresse mail. <br />Souhaitez vous lui envoyer un mail l'invitant à s'inscrire sur OMealShop ? <br />
                            <button id="btn-envoie-mail" type="button" class="btn btn-success">Oui</button>
                        </div>
                        <br/>
                        <ul id="user-list"></ul>
                        <br/>
                    </div>
                    <div id="demandes" style="display:none;">
                        <ul id="demandes-list"></ul>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Créer un foyer</h3>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-danger nomExiste" style="display:none;">
                            <strong>Erreur !</strong> Le nom de foyer que vous avez saisi est déjà utilisé.
                        </div>
                        <form id="creerFoyer" role="form">
                            <div class="form-group">
                                <label>Nom du foyer : </label><br/>
                                <div class="input-group custom-search-form">
                                    <input class="form-control" name="nom" placeholder="Nom du foyer">
                                    <span class="input-group-btn">
                                <button id="btnAddFoyer" type="button" class="btn btn-success">Créer foyer</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rechercher un foyer</h3>
                    </div>
                    <div class="panel-body">
                        <i class="fa fa-search-plus fa-fw"></i>
                        Vous pouvez chercher un foyer à partir du champ de recherche.
                        <div class="input-group custom-search-form">
                            <input class="form-control " id="searchFoyer" placeholder="Chercher un nom de foyer">
                            <span class="input-group-btn">
                                <button id="btnSearchFoyer" class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <br/>
                        <ul id="foyer-list"></ul>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <script src="../js/gestionFoyers.js"></script>
    <script src="../js/main.js"></script>

<?php
require_once '../view/footer.php';
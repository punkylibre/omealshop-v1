<?php
include_once "../view/nav.php";
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <div class="col-lg-10">
                <h1 class="page-header">Mot de passe oublié ?</h1>
                <div id="demandeReinitialisation" class="panel panel-danger">
                    <div class="panel-heading">
                        <strong>Quel est votre identifiant ?</strong> (mail)
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            Un mail vous sera envoyé avec un code pour changer votre mot de passe.
                        </div>
                        <div class="alert alert-warning">
                            Le code n'est valide que pour la session de navigation courante, veuillez ne pas quitter (ou
                            recharger) la page.
                        </div>
                        <div id="erreurIdentifiant" class="alert alert-danger" style="display: none;">

                        </div>
                        <label>Votre identifiant : </label>
                        <input class="form-control" type="text" id="identifiant"/>
                    </div>
                    <div class="panel-footer">
                        <button id="btnDemande" type="button" class="btn btn-outline btn-danger">Réinitialiser</button>
                    </div>
                </div>
                <div id="codeReinitialisation" class="panel panel-danger" style="display: none;">
                    <div class="panel-heading">
                        <strong>Saisissez votre code de réinitialisation</strong>
                    </div>
                    <div class="panel-body">
                        <label>Code de réinitialisation : </label>
                        <input class="form-control" type="text" id="codeReinit"/>
                    </div>
                    <div class="panel-footer">
                        <button id="btnCode" type="button" class="btn btn-outline btn-danger">Réinitialiser</button>
                    </div>
                </div>
                <div id="reinitialisation" class="panel panel-danger" style="display: none;">
                    <div class="panel-heading">
                        <strong>Veuillez saisir votre nouveau mot de passe</strong>
                    </div>
                    <div class="panel-body">
                        <div id="erreurPassword" class="alert alert-danger" style="display: none;">
                            <ul/>
                        </div>
                        <label>Votre nouveau mot de passe : </label>
                        <input class="form-control" type="password" id="motDePasse"/>
                        <label>Confirmer votre nouveau mot de passe : </label>
                        <input class="form-control" type="password" id="motDePasseConfirmation"/>
                    </div>
                    <div class="panel-footer">
                        <button id="btnReinitialisation" type="button" class="btn btn-outline btn-danger">
                            Réinitialiser
                        </button>
                    </div>
                </div>
                <div id="succesPassword" class="alert alert-success" style="display:none;">
                    Votre mot de passe à été réinitialisé avec succès.
                </div>
            </div>
            <!-- /.col-lg-10 -->

        </div>
        <!-- /.container-fluid -->
        <script src="../js/motDePasseOublie.js"></script>
        <script src="../js/main.js"></script>

<?php
require_once '../view/footer.php';
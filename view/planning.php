<?php
require_once "nav.php";
?>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul id="navigationBarre" class="nav navbar-nav side-nav" style="display: none;">
            <li>
                <a href="home.php"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
            </li>
            <li class="active" style="background-color: #1d1d1d !important;">
                <a href="planning.php" style="color: #FF6600 !important;"><i class="fa fa-fw fa-table" style="color: #FF6600 !important;"></i> Plannifier des repas</a>
            </li>
            <li>
                <a href="produits.php"><i class="fa fa-fw fa-edit"></i> Ajouter un produit</a>
            </li>
            <li>
                <a href="gestionRepas.php"><i class="fa fa-fw fa-wrench"></i> Gestion des repas</a>
            </li>
            <li>
                <a href="gestionListe.php"><i class="fa fa-fw fa-file"></i> Générer la liste</a>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <strong>Plannification :</strong> Plannifier vos repas
                    </h1>
                </div>
            </div>

            <div class="col-lg-12 text-center">
                <div class="panel panel-default">
                    <p>
                        <button id="moiPrecedent" type="button" class="btn btn-xs btn-primary">Mois précédent</button>
                        <label id="moisCourant"> Mois </label>
                        <label id="anneeCourante"> Annee</label>
                        <button id="moiSuivant" type="button" class="btn btn-xs btn-primary">Mois suivant</button>
                    </p>
                    <div id="calendrier" class="table-responsive">

                    </div>
                </div>
            </div>

            <div class="col-lg-6 text-center">
                <div class="panel panel-default">
                    <h3 class="page-header">Repas plannifié pour le :<br/>
                        <p class="jourSelect">Sélectionner une date </p></h3>
                    <div id="succesSuppr" class="alert alert-success" style="display: none;">
                        <strong>Succès!</strong> Votre repas a bien été supprimé.
                    </div>
                    <div id="erreurSuppr" class="alert alert-danger" style="display: none;">
                        <strong>Erreur!</strong> Il y a eu une erreur votre repas n'a pu être supprimé.
                    </div>
                    <h4>Petit déjeuner</h4>
                    <label> Petit déjeuner : </label>
                    <p id="planPetitDejeuner">aucun</p>
                    <h4>Déjeuner</h4>
                    <label> Entrée : </label>
                    <p id="planEntreeDejeuner">aucun</p>
                    <label> Plat de résistance : </label>
                    <p id="planPlatDejeuner">aucun</p>
                    <label> Dessert : </label>
                    <p id="planDessertDejeuner">aucun</p>
                    <h4>Diner</h4>
                    <label> Entrée : </label>
                    <p id="planEntreeDiner">aucun</p>
                    <label> Plat de résistance : </label>
                    <p id="planPlatDiner">aucun</p>
                    <label> Dessert : </label>
                    <p id="planDessertDiner">aucun</p>
                </div>
            </div>

            <div id="plannifieur" class="col-lg-6 text-center">
                <div class="panel panel-default">
                    <h3 class="page-header jourSelect">Sélection des menus</h3>
                    <div class="form-group">
                        <select id="lst-categorie-repas" name="lst-categorie-repas">
                            <option value="tout">Toutes les catégories</option>
                            <?php
                            require_once '../model/categorie_repas.php';
                            $categoriesRepas = categorie_repas::getAll();
                            var_dump($categoriesRepas);
                            foreach ($categoriesRepas as $cat)
                                echo '<option value="'.$cat->getId().'">'.$cat->getIntitule().'</option>';
                            ?>
                        </select><br />
                        <div class="checkbox">
                            <label>
                                <input id="no-date" value="" type="checkbox">Ajouter le menu sans date
                            </label>
                        </div>
                    </div>
                    <button id="boutonPetitDejeuner" type="button" class="btn btn-info">Petit-Déjeuner</button>
                    <button id="boutonDejeuner" type="button" class="btn btn-info">Déjeuner</button>
                    <button id="boutonDiner" type="button" class="btn btn-info">Diner</button>
                    <br/><br/>

                    <div id="succes" class="alert alert-success" style="display: none;">
                        <strong>Succès!</strong> Votre repas a bien été plannifié.
                    </div>
                    <div id="erreur" class="alert alert-danger" style="display: none;">
                        <strong>Erreur!</strong> Il y a eu une erreur votre repas n'a pu être plannifié.
                    </div>

                    <form id="petitDejeuner" method="post" action="../ctrl/planningRepas.php?moment=petitDejeuner"
                          role="form" style="display : none;">
                        <div class="form-group">
                            <h3 class="page-header">Le petit-déjeuner</h3>
                            <select name="petitDejeuner" class="form-control petit-dejeuner">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonnePetitDejeuner" class="form-control nbPersonne">

                            </select><br/>
                            <input type="hidden" class="date" name="date">
                            <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                        </div>
                    </form>

                    <form id="dejeuner" method="post" action="../ctrl/planningRepas.php?moment=dejeuner" role="form"
                          style="display : none;">
                        <div class="form-group">
                            <h3 class="page-header">Le déjeuner</h3>
                            <label>L'entrée </label>
                            <select name="entree" class="form-control entree">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonneEntree" class="form-control nbPersonne">

                            </select><br/>

                            <label>Le plat de résistance </label>
                            <select name="platDejeuner" class="form-control plat">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonnePlat" class="form-control nbPersonne">

                            </select><br/>

                            <label>Le dessert </label>
                            <select name="dessert" class="form-control dessert">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonneDessert" class="form-control nbPersonne">

                            </select><br/>
                            <input type="hidden" class="date" name="date">
                            <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                        </div>
                    </form>

                    <form id="diner" method="post" action="../ctrl/planningRepas.php?moment=diner" role="form"
                          style="display : none;">
                        <div class="form-group">
                            <h3 class="page-header">Le diner</h3>
                            <label>L'entrée </label>
                            <select name="entree" class="form-control entree">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonneEntree" class="form-control nbPersonne">

                            </select><br/>

                            <label>Le plat de résistance </label>
                            <select name="plat" class="form-control plat">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonnePlat" class="form-control nbPersonne">

                            </select><br/>

                            <label>Le dessert </label>
                            <select name="dessert" class="form-control dessert">
                                <option value="aucun">Aucun</option>
                            </select>
                            <label>Pour </label>
                            <select name="nbPersonneDessert" class="form-control nbPersonne">

                            </select><br/>
                            <input type="hidden" class="date" name="date">
                            <button id="valider" type="submit" class="btn btn-primary">Valider</button>
                        </div>
                    </form>
                    <button id="enrouler" type="button" class="btn btn-xs btn-link -align-right">enrouler</button>
                </div>
            </div>

            <div id="repas-sans-date" class="col-lg-12 text-center">
                <div class="panel panel-default">
                    <h3 class="page-header">Plats plannifié sans dates</h3>
                    <div id="succesSupprSansDate" class="alert alert-success" style="display: none;">
                        <strong>Succès!</strong> Votre repas a bien été supprimé.
                    </div>
                    <div id="erreurSupprSansDate" class="alert alert-danger" style="display: none;">
                        <strong>Erreur!</strong> Il y a eu une erreur votre repas n'a pu être supprimé.
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nom du plat</th>
                                <th>Nombre de personnes</th>
                                <th>Plannifieur</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody id="tbl-repas-sans-date"/>

                    </table>
                </div>

            </div>
            <!-- /.container-fluid -->

            <script src="../js/planning.js"></script>
            <script src="../js/main.js"></script>
<?php
include_once 'footer.php';
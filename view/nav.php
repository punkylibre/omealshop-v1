<?php
session_start();
require_once('../model/personne.php');
require_once('../model/session.php');
require_once('../model/foyer.php');
if (isset($_SESSION['session'])) {
    $session = unserialize($_SESSION['session']);
    $personne = $session->getPersonne();
    $foyer = $session->getFoyer();
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <link rel="icon" type="image/png" href="../image/favicon.png" />
<!--    <link rel="icon" type="image/x-icon" href="../image/favicon.ico" />-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OMealShop</title>


    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><img id="logo" src="../image/logo.png" height="50px"/> </a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <form id="connexion" method="post" action="../ctrl/connexion.php" style="display : none;">
                <p><label>Identifiant
                        <input name="username" type="text" required></label>
                    <label>Mot de passe
                        <input name="password" type="password" required></label>
                    <button type="submit" class="btn btn-sm btn-success">Se connecter</button>
                    <button id="inscription" type="button" class="btn btn-sm btn-primary">S'inscrire</button>
                    <button id="passwordForgotten" type="button" class="btn btn-sm btn-link" style="color: #FF6600;">Mot
                        de passe oublié ?
                    </button>
                </p>
            </form>
            <li id="foyer" class="dropdown connecte" style="display : none;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                            class="fa fa-home"></i> <?php echo ($foyer != null) ? $foyer->getNom() : 'Foyer'; ?><b
                            class="caret"></b></a>
                <ul id="foyers" class="dropdown-menu">
                    <?php
                    if ($personne != null) {
                        $foyers = membre_foyer::getFoyersPersonne($personne->getId());
                        if (sizeof($foyers) > 0)
                            foreach ($foyers as $foyer) {
                                echo '<li><button class="ln-foyer btn-link" value="' . $foyer->getId() . '"><i class="fa fa-fw fa-users"></i> ' . $foyer->getNom() . '</button></li>';
                            }
                    }
                    ?>
                    <li class="divider"></li>
                    <li>
                        <a href="../view/gestionFoyer.php"><i class="fa fa-fw fa-gears"></i> Gestion des foyers</a>
                    </li>
                </ul>
            </li>

            <li id="profil" class="dropdown connecte" style="display : none;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                            class="fa fa-user"></i> <?php if (isset($personne)) echo $personne->getPrenom() . ' ' . $personne->getNom(); ?>
                    <b class="caret"></b></a>
                <ul id="monProfil" class="dropdown-menu">
                    <li>
                        <a href="../view/parametres.php"><i class="fa fa-fw fa-gear"></i> Paramètres</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="charteUtilisation.php"><i class="fa fa-fw fa-book"></i>
                            Charte d'utilisation</a>
                    </li>
                    <li>
                        <a id="deconnexion"><i class="fa fa-fw fa-power-off"></i>
                            Déconnexion</a>
                    </li>
                </ul>
            </li>

        </ul>
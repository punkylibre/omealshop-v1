<?php
require_once '../model/personne.php';
require_once '../model/session.php';

$drapeau = false;
$id = $_GET['id'];

$personne = personne::getById($id);
if ($personne->getEtat() == 1)
    $retour = true;
else
    $retour = $personne->activerCompte();
if ($retour) {
    $personne = personne::getById($id);
    $session = new session($personne);
    $_SESSION['session'] = serialize($session);
    header('Location: ../index.php');
} else
    echo 'Erreur ! Votre compte n\'a pu être activé';
<?php
session_start();
require_once '../model/DB.php';
require_once  '../model/personne.php';
require_once  '../model/repas.php';
require_once  '../model/repas_plannifie.php';
require_once  '../model/session.php';
require_once  '../model/foyer.php';
$session = unserialize($_SESSION['session']);
$personne = $session->getPersonne();
$foyer = $session->getFoyer();

if (isset($_SESSION['session']) && isset($_GET['type']) && isset($_GET['cat'])) {
    $type = $_GET['type'];
    $cat = $_GET['cat'];
    $repas = repas::getRepasByTypeAndCat($type, $cat);
}
else if(isset($_SESSION['session']) && isset($_GET['date'])) {
    $date = $_GET['date'];
    if($date == "2000-01-01"){
        $dateDeb = $date.' 0:0:0';
        $dateFin = $date.' 0:0:5';
    }
    else{
        $dateDeb = $date.' 8:0:0';
        $dateFin = $date.' 23:0:0';
    }
    $repasPlannifie = repas_plannifie::getRepasBetweenDate($dateDeb, $dateFin, $foyer);

}
else{
    header("Location: ../home.php");
}

$obj = new stdClass();
$obj->repas = Array();
$obj->repasPlanifie = Array();
if(count($repas) > 0)
    foreach ($repas as $rep) {
        array_push($obj->repas, '{"idRepas": '.$rep->getId().', "nomRepas": "'.$rep->getNom().'"}');
    }
if(count($repasPlannifie) > 0)
    foreach ($repasPlannifie as $rep) {
        array_push($obj->repasPlanifie, '{"idRepas": '.$rep->getRepas()->getId(). ', "nomRepas": "'.$rep->getRepas()->getNom().
            '", "nbPersonne": '.$rep->getNbPersonnes(). ', "dateRepas": "'.date_format($rep->getDate(), 'Y-m-d H:i:s').
            '","nomDemandeur": "'.$rep->getDemandeur()->getNom().'", "prenomDemandeur": "'.$rep->getDemandeur()->getPrenom().
            '", "idPlan": '.$rep->getId().' }');
    }



////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
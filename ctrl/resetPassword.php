<?php
/**************************************************************************
 *Créateur:		Alexis POYEN																									*
 *Date:				14/04/17																											*
 *Description: 	Ce fichier permet de modifier le mot de passe de l'utilisateur à sa dmeande	*
 ***************************************************************************/
session_start();
include_once '../model/personne.php';
$personne = personne::getByMail($_GET['log']);

$drapeau=false;

/**************************************************************************************
 *On fait les même vérifications sur le mot de passe que pour l'inscription avant de faire le changement		*
 ***************************************************************************************/
if(isset($_GET['mdp']) && isset($_GET['confirm'])){  //si tous les champs obligatoire ont été renseignés
    $drapeau = true;  //pour l'instant on peut modifier le mot de passe

    if($_GET['mdp'] != $_GET['confirm']){
        $AErreurInscription[] = 'Les mot de passes ne coresspondent pas';
        $drapeau = false;
    }

    /**********************************************************************
     *Si tout est ok on essaie de changer le mot de passe si échec on en avertit l'utilisateur	*
     ***********************************************************************/
    if($drapeau){
        $AErreurInscription = array();
        if($personne->changerMotDePasse($_GET['mdp'])){
            $drapeau = true;
        }
        else{
            $AErreurInscription[] = 'Votre mot de passe n\'a pas été modifié dans la base de données';
            $drapeau  = false;
        }
    }

}
else{
    $AErreurInscription[] = 'Vous n\'avez pas renseigné les champs de mot de passe';
    $drapeau = false;
}

/****************************************************************************************
 *On envoie si le changement a bien été effectué ou non et les erreurs qui peuvent être la cause de l'échec	*
 *****************************************************************************************/
$obj = new stdClass();
$obj->ok = $drapeau;
$obj->message = Array();
if(count($AErreurInscription) > 0)
    foreach ($AErreurInscription as $erreur){
        array_push($obj->message, $erreur);
    }

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
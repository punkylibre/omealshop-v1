<?php
require_once '../model/personne.php';

if(isset($_POST['mdp'])
    && isset($_POST['mail'])
    && isset($_POST['nom'])
    && isset($_POST['prenom'])
    && isset($_POST['mdpConfirm'])){
    $drapeau = true;
    $mail = $_POST['mail'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $mdp = $_POST['mdp'];
    $mdpConfirm = $_POST['mdpConfirm'];

    /********************************************************************
     * Vérification du format du mail et qu'il n'existe pas déjà en BD	*
     * Règles de gestion :   1.1                                        *
     ********************************************************************/
    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)){
        $mailValide = false;
        $drapeau = false;
    }
    else
        $mailValide = true;

    if(personne::getByMail($mail) != false){
        $mailExiste = false;
        $drapeau  = false;
    }
    else
        $mailExiste = true;
    /********************************************************************
     * Vérification que les noms, prénoms ne contiennent pas de chiffres*
     ********************************************************************/
    if(preg_match('#[0-9]#',$nom) > 0){
        $prenomValide = false;
        $drapeau = false;
    }
    else
        $prenomValide = true;

    if(preg_match('#[0-9]#',$nom) > 0){
        $nomValide = false;
        $drapeau = false;
    }
    else
        $nomValide = true;
    /********************************************************************
     * Vérifier que les deux mot de passes saisis correspondent     	*
     ********************************************************************/
    if($mdp != $mdpConfirm){
        $mdpIdentique = false;
        $drapeau = false;
    }
    else
        $mdpIdentique = true;

    /****************************************************
     * Si tout est ok ajout en bd de l'utilisateur      *
     ****************************************************/
    if($drapeau)
        $retour = personne::inscription($nom, $prenom, $mail, $mdp);
    else{
        $retour = false;

    }
}
else
    $retour = false;

/********************************************************************
 * Renvoie en JSON de la valeur de retour 	                        *
 ********************************************************************/
$obj = new stdClass();
$obj->ok = $retour;
$obj->mailExiste = $mailExiste;
$obj->mailValide = $mailValide;
$obj->prenomValide = $prenomValide;
$obj->nomValide = $nomValide;
$obj->mdpIdentique = $mdpIdentique;

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);
?>
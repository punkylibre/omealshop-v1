<?php
session_start();
require_once '../model/DB.php';
require_once '../model/personne.php';
require_once '../model/repas.php';
require_once '../model/repas_plannifie.php';
require_once '../model/session.php';
require_once '../model/foyer.php';

$drapeau = true;

$session = unserialize($_SESSION['session']);
$personne = $session->getPersonne();
$foyer = $session->getFoyer();

if (isset($_SESSION['session'])) {
    $idUser = $personne->getId();
    $moment = $_GET['moment'];
    $dateCourante = new DateTime(date('Y-m-d H:i:s'));

    if ($moment == 'ajout') {
        $date = $_GET['date'];
        $nbPersonne = $_GET['nb'];
        $idRepas = $_GET['id'];
        $drapeau = repas_plannifie::updateRepas($foyer, $date, $nbPersonne, repas::getById($idRepas));
    } else if ($moment == 'supprimer') {
        if (isset($_GET['date'])) {
            $date = $_GET['date'];
            $drapeau = repas_plannifie::supprRepas($foyer, $date);
        } elseif (isset($_GET['id'])) {
            $id = $_GET['id'];
            $repas = repas_plannifie::getById($id);
            $drapeau = $repas->supprimer();
            $idPlan = $id;

        }
    } else if ($moment == 'petitDejeuner') {
        if (isset($_POST['petitDejeuner']) && $_POST['petitDejeuner'] != 'aucun' && isset($_POST['nbPersonnePetitDejeuner']) && $_POST['petitDejeuner'] != 'aucun') {
            $idPetitDejeuner = $_POST['petitDejeuner'];
            $nbPersonne = $_POST['nbPersonnePetitDejeuner'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 8:0:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idPetitDejeuner, $nbPersonne, "petit déjeuner"))
                        $drapeau = false;
                } else {
                    $drapeau = false;
                    $erreur[] = "Aucune date n'a été sélectionnée";
                }
            } else {
                $date = '2000-01-01 00:00:00';
                $dateSaisie = new DateTime($date);
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idPetitDejeuner), $personne, $dateSaisie, $nbPersonne);
            }

        }

    } else if ($moment == 'dejeuner') {
        $drapeau = true;
        if (isset($_POST['entree'])  && $_POST['entree'] != 'aucun' && isset($_POST['nbPersonneEntree']) && $_POST['entree'] != 'aucun') {
            $idEntree = $_POST['entree'];
            $nbPersonne = $_POST['nbPersonneEntree'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 12:0:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idEntree, $nbPersonne, 'entrée'))
                        $drapeau = false;
                } else {
                    $drapeau = false;
                    $erreur[] = "Aucune date n'a été sélectionnée";
                }
            } else {
                $date = '2000-01-01 00:00:00';
                $dateSaisie = new DateTime($date);
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idEntree), $personne, $dateSaisie, $nbPersonne);
            }

        }
        $drapeau = true;

        if (isset($_POST['platDejeuner']) && $_POST['platDejeuner'] != 'aucun' && isset($_POST['nbPersonnePlat']) && $_POST['platDejeuner'] != 'aucun') {
            $idPlat = $_POST['platDejeuner'];
            $nbPersonne = $_POST['nbPersonnePlat'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 12:30:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idPlat, $nbPersonne, 'plat principal'))
                        $drapeau = false;
                } else {
                    $drapeau = false;
                    $erreur[] = "Aucune date n'a été sélectionnée";
                }
            } else {
                $date = '2000-01-01 00:00:00';
                $dateSaisie = new DateTime($date);
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idPlat), $personne, $dateSaisie, $nbPersonne);
            }

        }
        $drapeau = true;

        if (isset($_POST['dessert']) && $_POST['dessert'] != 'aucun' && isset($_POST['nbPersonneDessert']) && $_POST['dessert'] != 'aucun') {
            $idDessert = $_POST['dessert'];
            $nbPersonne = $_POST['nbPersonneDessert'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 13:0:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idDessert, $nbPersonne, 'dessert'))
                        $drapeau = false;
                } else {
                    $date = '2000-01-01 00:00:00';
                    $dateSaisie = new DateTime($date);
                }
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idDessert), $personne, $dateSaisie, $nbPersonne);
            }

        }
    } else if ($moment == 'diner') {
        $drapeau = true;
        if (isset($_POST['entree']) && $_POST['entree'] != 'aucun' && isset($_POST['nbPersonneEntree']) && $_POST['entree'] != 'aucun') {
            $idEntree = $_POST['entree'];
            $nbPersonne = $_POST['nbPersonneEntree'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 19:0:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idEntree, $nbPersonne, 'entrée'))
                        $drapeau = false;
                } else {
                    $date = '2000-01-01 00:00:00';
                    $dateSaisie = new DateTime($date);
                }
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idEntree), $personne, $dateSaisie, $nbPersonne);
            }

        }
        $drapeau = true;

        if (isset($_POST['plat']) && $_POST['plat'] != 'aucun' && isset($_POST['nbPersonnePlat']) && $_POST['plat'] != 'aucun') {
            $idPlat = $_POST['plat'];
            $nbPersonne = $_POST['nbPersonnePlat'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 19:30:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idPlat, $nbPersonne, "plat principal"))
                        $drapeau = false;
                } else {
                    $date = '2000-01-01 00:00:00';
                    $dateSaisie = new DateTime($date);
                }
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idPlat), $personne, $dateSaisie, $nbPersonne);
            }

        }
        $drapeau = true;

        if (isset($_POST['dessert']) && $_POST['dessert'] != 'aucun' && isset($_POST['nbPersonneDessert']) && $_POST['dessert'] != 'aucun') {
            $idDessert = $_POST['dessert'];
            $nbPersonne = $_POST['nbPersonneDessert'];
            if ($_GET['date'] == 0) {
                if (sizeof($_POST['date']) >= 1) {
                    $date = $_POST['date'];
                    $date .= ' 20:0:0';
                    $dateSaisie = new DateTime($date);
                    if ($dateCourante > $dateSaisie) {
                        $erreur[] = "La date sélectionnée est antérieure à la date actuelle";
                        $drapeau = false;
                    }

                    if (isRepasExist($date, $idDessert, $nbPersonne, 'dessert'))
                        $drapeau = false;
                } else {
                    $date = '2000-01-01 00:00:00';
                    $dateSaisie = new DateTime($date);
                }
            }

            if ($drapeau) {
                $drapeau = repas_plannifie::addRepas($foyer, repas::getById($idDessert), $personne, $dateSaisie, $nbPersonne);
            }

        }
    }

} else {
    header('Location: ../home.php');
}

$repasPlannifies;
function isRepasExist($date, $idRepas, $nbPersonne, $type)
{
    global $repasPlannifies;
    $session = unserialize($_SESSION['session']);
    $foyer = $session->getFoyer();
    $repasPlannifie = repas_plannifie::repasAlreadyPlanned($foyer, $date);
    if ($repasPlannifie != null) {
        $newRepas = repas::getById($idRepas);
        if ($newRepas->getId() == $repasPlannifie->getRepas()->getId() && $nbPersonne == $repasPlannifie->getNbPersonnes()) {
            return false;
        } else {
            $repasPlannifies[] = '{"typeRepas": "' . $type . '", "ancienRepas": "' . $repasPlannifie->getRepas()->getNom()
                . '", "nbPersonne": ' . $repasPlannifie->getNbPersonnes() . ', "nouveauRepas" :"' . $newRepas->getNom()
                . '", "idNouveauRepas": ' . $idRepas . ', "newNbPersonne":' . $nbPersonne
                . ', "date": "' . $date . '"}';
            return true;
        }
    }
    return false;
}

$obj = new stdClass();
$obj->ok = $drapeau;
$obj->erreur = Array();
$obj->repasPlannifie = Array();
if (count($erreur) > 0)
    foreach ($erreur as $e) {
        array_push($obj->erreur, $e);
    }
if (count($repasPlannifies) > 0)
    foreach ($repasPlannifies as $repas)
        array_push($obj->repasPlannifie, $repas);
if (isset($idPlan)) {
    $obj->idPlan = $idPlan;
}

////////////Sorties des variables en JSON
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($obj);